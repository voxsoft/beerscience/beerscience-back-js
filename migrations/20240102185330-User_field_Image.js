'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('Users', 'imageId', {
      type: Sequelize.INTEGER,
      references: {
        model: 'FileReferences',
        key: 'id',
      }
    })
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.removeColumn(
      'Users',
      'imageId'
    );
  }
};
