'use strict';
const path = require('path');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    const {brewerySeeds} = await import('../src/data/brewerySeeds.mjs');
    await queryInterface.bulkInsert('Breweries', brewerySeeds, {});
    await queryInterface.sequelize.query(`select setval('"Breweries_id_seq"', (select max(id) from public."Breweries"), true);`);
  },

  async down(queryInterface, Sequelize) {
    const Op = Sequelize.Op
    await queryInterface.bulkDelete('Breweries', {id: {[Op.between] : [1, 100]}}, {});
  }
};
