'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Users', [{
      id: 1,
      name: 'Angela Smith',
      passHash: '',
      login: 'angelasmith',
      email: 'angelasmith@mock.email',
      role: 'USER',
      createdAt: '2023-12-05T14:10:00Z',
      updatedAt: '2023-12-08T09:55:00Z'
    },
    {
      id: 2,
      name: 'May Wallas',
      passHash: '',
      login: 'maywallas',
      email: 'maywallas@mock.email',
      role: 'ADMIN',
      createdAt: '2023-03-15T12:45:00Z',
      updatedAt: '2023-03-17T08:30:00Z'
    },
    {
      id: 3,
      name: 'Andrew Jay',
      passHash: '',
      login: 'andreyjay',
      email: 'andrewjay@mock.email',
      role: 'USER',
      createdAt: '2023-03-15T12:45:00Z',
      updatedAt: '2023-03-17T08:30:00Z'
    }], {});
    await queryInterface.sequelize.query(`select setval('"Users_id_seq"', (select max(id) from public."Users"), true);`);
  },

  async down (queryInterface, Sequelize) {
    const Op = Sequelize.Op
    await queryInterface.bulkDelete('Users', {id: {[Op.between] : [1, 3]}}, {});
  }
};
