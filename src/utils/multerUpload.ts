const multer = require("multer");
const path = require("path");
const fs = require("fs");

const UPLOAD_FOLDER =  path.join(__dirname, '..', '..', 'public', 'upload');

if (!fs.existsSync(UPLOAD_FOLDER)) {
    fs.mkdirSync(UPLOAD_FOLDER, { recursive: true });
}

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, UPLOAD_FOLDER)
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + "-" + Date.now()+".jpg")
    }
  })

const maxSize = 1 * 10000000; // 10MB

export const upload = multer({ 
    storage: storage,
    limits: { fileSize: maxSize },
    fileFilter: function (req, file, cb){
        const filetypes = /jpeg|jpg|png/;
        const mimetype = filetypes.test(file.mimetype);
  
        const extname = filetypes.test(path.extname(
                    file.originalname).toLowerCase());
        
        if (mimetype && extname) {
            return cb(null, true);
        }
      
        cb("Error: File upload only supports the "
                + "following filetypes - " + filetypes);
      } 
});