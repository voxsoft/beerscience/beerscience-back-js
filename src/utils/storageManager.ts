import { AccessStatus, FileReference, StorageType } from "../models/FileReference";

const {Storage} = require('@google-cloud/storage');

// For more information on ways to initialize Storage, please see
// https://googleapis.dev/nodejs/storage/latest/Storage.html

// Creates a client using Application Default Credentials
const storage = new Storage();

// Creates a client from a Google service account key
// const storage = new Storage({keyFilename: 'key.json'});

/**
 * TODO(developer): Uncomment these variables before running the sample.
 */
// The ID of your GCS bucket
const bucketName = 'beerit-de-bucket';

interface GcloudFile {
    metadata: {
        kind: string,// 'storage#object',
        id: string, //'beerit-de-bucket/image/1686775320985080',
        selfLink: string, //'https://www.googleapis.com/storage/v1/b/beerit-de-bucket/o/image',
        mediaLink: string, //'https://storage.googleapis.com/download/storage/v1/b/beerit-de-bucket/o/image?generation=1686775320985080&alt=media',
        name: string, //'image',
        bucket: string, //'beerit-de-bucket',
        generation: string, //'1686775320985080',
        metageneration: string, //'1',
        storageClass: string, //'STANDARD',
        size: number, //20921,
        md5Hash: string, //'C1FsE1t/ZAzw0KtewQho8Q==',
        crc32c: string, //'RXTQzw==',
        etag: string, //'CPj7yt7Pw/8CEAE=',
        timeCreated: string, //'2023-06-14T20:42:00.998Z',
        updated: string, //'2023-06-14T20:42:00.998Z',
        timeStorageClassUpdated: string //'2023-06-14T20:42:00.998Z'
    }
};

async function createBucket() {
  // Creates the new bucket
  await storage.createBucket(bucketName);
  console.log(`Bucket ${bucketName} created.`);
}

// createBucket().catch(console.error);

export const saveFile = async ({filename, path}) => {
    const options = {
        destination: filename
    };

    console.log('try to save');

    try {
        console.time('gcloudUpload');
        const result = await storage.bucket(bucketName).upload(path, options);
        console.timeEnd('gcloudUpload');
        if (result.length === 0) return {err: 'Error writing file'};
        console.log(result[0].metadata);
        const savedFileData: GcloudFile = result[0];
        const innerFile: FileReference = await FileReference.create({
            name: filename,
            storageType: StorageType.GCLOUD,
            storageId: savedFileData.metadata.id,
            metadata: savedFileData.metadata,
            accessStatus: AccessStatus.PRIVATE,
            userId: 0,
            url: `https://storage.googleapis.com/beerit-de-bucket/${filename}`
        });
        return {file: innerFile.toJSON()};
    } catch(err) {
        console.log('err', err);
        return {err: err};
    }

}