import * as bodyParser from 'body-parser';
import * as express from 'express';
// import * as cors from 'cors';
// import * as expressJwt from 'express-jwt';
// import * as ExpressSession from 'express-session';
import { createServer, Server } from 'http';
import * as path from 'path';
import { dbConnect } from './db';

import { StatusController } from './controllers/status';
import { BeerController } from './controllers/beer';
import { FileController } from './controllers/file';
import { EntityController } from './controllers/entity';
import { AuthController, authJWT } from './controllers/auth';

export class AppServer {
  private app: express.Application;
  private server: Server;
  private port: string | number;

  constructor() {
    this.createApp();
    this.config();
    this.createServer();
    this.databases()
      .then(res => {
        console.log('Check database');
      })
      .catch(err => {
        if (process.env.DB_NOT_CRASH) {
          console.log('Database not ready.');
        } else {
          throw err;
        }
      });
    this.addRoutes();
    this.listen();
  }

  public getApp(): express.Application {
    return this.app;
  }

  private createApp(): void {
    this.app = express();

    this.app.set('view engine', 'ejs');
    this.app.use(bodyParser.urlencoded({ extended: true, limit: '50Mb' }));

    this.app.use(bodyParser.json());

    // this.app.options('*', cors());
    // this.app.use(cors({
    //   credentials: true,
    //   optionsSuccessStatus: 200
    // }));

    // this.app.use(
    //   ExpressSession({
    //     secret: process.env.SALT,
    //     resave: false,
    //     saveUninitialized: true,
    //   })
    // );

    // this.app.set('views', path.join(__dirname, '../templates'));

    this.app.use((req, res, next) => {
      // console.log(req.url);
      // console.log(req.method);
      next();
    });

    // Add CORS
    this.app.all('*', authJWT, (req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header(
        'Access-Control-Allow-Methods',
        'GET,PUT,POST,DELETE,OPTIONS,PATCH'
      );
      res.header('Access-Control-Allow-Headers', '*');
      res.header('Access-Control-Allow-Credentials', 'true');
      if (req.method === 'OPTIONS') {
        res.status(200).end();
      } else {
        next();
      }
    });

  }

  private addRoutes(): void {
    this.app.use('/status', StatusController);
    this.app.use('/beer', BeerController);
    this.app.use('/file', FileController);
    this.app.use('/entity', EntityController);
    this.app.use('/auth', AuthController)

    // Static
    this.app.use(express.static(path.join(__dirname, '..', 'assets')));
    this.app.use(express.static(path.join(__dirname, '..', 'public')));
  }

  private createServer(): void {
    this.server = createServer(this.app);
  }

  private config(): void {
    this.port =
      typeof process.env.PORT !== 'undefined' ? process.env.PORT : 9000;
  }

  private async databases() {
    await dbConnect();
  }

  private listen(): void {
    this.server.listen(this.port, () => {
      console.log('Running server on port %s', this.port);
    });
  }
}