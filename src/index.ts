import * as dotenv from 'dotenv-extended';
dotenv.load();

import { AppServer } from './server';


const server = new AppServer();
const app = server.getApp();
export { app, server };
