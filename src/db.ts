import * as path from 'path';
import { Model, Op } from 'sequelize';
import { Sequelize } from 'sequelize-typescript';

const sequelize = new Sequelize({
    database: process.env.POSTGRES_DB,
    dialect: 'postgres',
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    host: process.env.POSTGRES_HOST ? process.env.POSTGRES_HOST : 'localhost',
    modelPaths: [__dirname + '/models'],
    operatorsAliases: { $like: Op.iLike, $gte: Op.gte, $between: Op.between, $or: Op.or },
    port: Number(process.env.POSTGRES_PORT ? process.env.POSTGRES_PORT : 5432),
    logging: process.env.NODE_ENV === 'development' ? true : false
});

export const dbConnect = async () =>
  new Promise<any>((resolve, rej) => {
    sequelize
        .sync() // { force: true }
        .then(res => {
            console.log('Database sync');
            resolve('');
        })
        .catch(err => {
            console.error(err);
            rej(err);
        });
  });

export const getSequelize = () =>
  sequelize;

// export const getModel = (model: string) => {
//   const modelName = model.charAt(0).toUpperCase() + model.slice(1);
//   const supplierModel: any = sequelize.modelManager.getModel(modelName);
//   return supplierModel;
// };
