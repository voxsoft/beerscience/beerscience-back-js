export const withSort = (req, res, next) => {
    let sortQuery = req.query.sort;
    req.sort = {};
    if (!sortQuery) {
        next();
    } else {
        const result = [];
        if (typeof sortQuery === 'string') {
            try {
                sortQuery = JSON.parse(sortQuery);
            } catch(err) {
                console.log(err);
            }
        } 

        if (typeof sortQuery[Symbol.iterator] === 'function') {
            for (const item of sortQuery) {
                if (typeof item === 'string') {
                    try {
                        const parsed = JSON.parse(sortQuery);
                        result.push(parsed);
                    } catch(err) {
                        continue;
                    }
                } else {
                    result.push(item);
                }
            }
        } 
        if (result.length > 0) {
            req.sort = {order: result};
        }
        next();
    }
}