export const withWhere = (req, res, next) => {
    const whereQuery = req.query.where;
    req.where = {};
    if (!whereQuery) {
        next();
    } else {
        if (typeof whereQuery === 'string') {
            try {
                const parsed = JSON.parse(whereQuery);
                req.where = { where: parsed };
            } catch(err) {
                console.log(err);
            }
        } else {
            req.where = {where: req.query.where};
        }
        next();
    }
}