export const withPagination = (req, res, next) => {
    req.pagination = {};
    if (!req.query.pagination) {
        next();
    } else {
        let paginationQuery = req.query.pagination;
        if (typeof paginationQuery === 'string') {
            try {
                paginationQuery = JSON.parse(paginationQuery);
            } catch (err) {
                req.pagination = {};
                return next();
            }
        }
        req.pagination = {
            offset: paginationQuery.page ? paginationQuery.page * paginationQuery.perPage : 0,
            limit: paginationQuery.perPage ? paginationQuery.perPage : undefined
        };
        next();
    }
}
