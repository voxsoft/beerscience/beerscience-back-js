import { getSequelize } from "../db";

export const withInclude = (req, res, next) => {
    const includeQuery = req.query.include;
    req.include = {};
    if (!includeQuery) {
        next();
    } else {
        const sequelize = getSequelize();
        if (includeQuery) {
            const includesArray = [];
            for (const item of includeQuery) {
                if (item.indexOf('{') !== -1) {
                    try {
                        const parsed = JSON.parse(item);
                        if (parsed.model) {
                            const includeModel = sequelize.models[parsed.model];
                            if (includeModel) {
                                parsed.model = includeModel;
                                includesArray.push(parsed);
                            }
                        }
                    } catch(err) {}
                } else {
                    const includeModel = sequelize.models[item];
                    if (includeModel) {
                        includesArray.push({model: includeModel});
                    }
                }
            }
            if (includesArray.length > 0) {
                req.include = {include: includesArray};
            }
        }
        next();
    }
}