import {
  AutoIncrement,
  BelongsTo,
  Column,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
  Default,
  DataType,
} from 'sequelize-typescript';
import { User } from './User';

export interface BeerStyleInterface {
  name: string;
}
@Table({ timestamps: false })
export class BeerStyle extends Model<BeerStyle> {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  name: string;

  @Column({ type: DataType.FLOAT })
  minABV?: number;

  @Column({ type: DataType.FLOAT })
  maxABV?: number;

  @Column({ type: DataType.FLOAT })
  minIBU?: number;

  @Column({ type: DataType.FLOAT })
  maxIBU?: number;

  @Column({ type: DataType.TEXT })
  description: string;

  @Default(false)
  @Column({ type: DataType.BOOLEAN })
  moderated: boolean;

  @Column({ type: DataType.ARRAY(DataType.STRING) })
  typing: string[];

  @ForeignKey(() => User)
  @Column
  userId: number;

  @BelongsTo(() => User)
  user: User;
}