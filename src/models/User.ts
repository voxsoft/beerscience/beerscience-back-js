import {
    AutoIncrement,
    BelongsTo,
    Column,
    DataType,
    ForeignKey,
    Model,
    PrimaryKey,
    Table,
} from 'sequelize-typescript';
import { FileReference, FileReferenceInterface } from './FileReference';

export enum Role {
    USER = 'USER',
    ADMIN = 'ADMIN'
}

export interface UserInterface {
    id: number;
    name: string;
    passHash: string;
    login: string;
    googleId?: string;
    email: string;
    role: Role;
    imageId: number;
    image?: FileReferenceInterface;
    createdAt: any;
    updatedAt: any;
}

@Table
export class User extends Model<User> {
    @PrimaryKey
    @AutoIncrement
    @Column
    id: number;
  
    @Column
    name: string;

    @Column
    passHash: string;

    @Column
    login: string;

    @Column
    googleId: string;

    @Column
    email: string;

    @Column(DataType.ENUM({ values: Object.keys(Role) }))
    role: Role;

    @ForeignKey(() => FileReference)
    @Column
    imageId: number;
    
    @BelongsTo(() => FileReference)
    image: FileReference;

    @Column({type: DataType.DATE})
    createdAt: any;

    @Column({type: DataType.DATE})
    updatedAt: any;
}