import {
    AutoIncrement,
    Column,
    DataType,
    ForeignKey,
    Model,
    PrimaryKey,
    Table,
} from 'sequelize-typescript';

export enum StorageType {
    GCLOUD = 'GCLOUD',
    LOCAL = 'LOCAL'
}

export enum AccessStatus {
    PRIVATE = 'PRIVATE',
    PUBLIC = 'PUBLIC'
}

export interface FileReferenceInterface {
    id: number;
    name: string;
    userId: number;
    url: string;
}

@Table
export class FileReference extends Model<FileReference> {
    @PrimaryKey
    @AutoIncrement
    @Column
    id: number;
  
    @Column
    name: string;

    @Column(DataType.ENUM({ values: Object.keys(StorageType) }))
    storageType: StorageType;

    @Column
    storageId: string;

    @Column(DataType.JSON)
    metadata: any

    @Column(DataType.ENUM({ values: Object.keys(AccessStatus) }))
    accessStatus: AccessStatus

    @Column
    userId: number

    @Column
    url: string;

}