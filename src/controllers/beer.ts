import { Router } from 'express';
import { TestedBeer } from '../models/TestedBeer';
import { BeerStyle } from '../models/BeerStyle';
import { Brewery } from '../models/Brewery';
import { FileReference } from '../models/FileReference';
import { User } from '../models/User';
import { listMW } from '../middleware/list';

const router: Router = Router();

router.get('/tested/user/list/:id', ...listMW, async (req, res) => {
    try {
        let where = req.where;
        if (!where.where) {
            where = {where: {userId: req.params.id}}
        } else {
            where.where['userId'] = req.params.id;
        }

        const include = [
            { model: BeerStyle, attributes: ['name'] }, 
            { model: Brewery }, 
            { model: FileReference},
            { model: User, 
                include: [{model: FileReference, attributes: ['url', 'id']}]
            }
        ];

        const result = await TestedBeer.findAll({
            include,
            ...where,
            ...req.sort,
            ...req.pagination
        });
        const total = await TestedBeer.count({
            include,
            ...where,
            ...req.sort
        });
        res.json({ items: result, totalItems: total });
    } catch (error) {
        res.status(500).send(error.message);
    }
});

router.get('/tested/my/list', ...listMW, async (req, res) => {
    try {
        if (!req.user) return res.json({items: [], totalItems: 0});
        let where = req.where;
        if (!where.where) {
            where = {where: {userId: req.user.id}}
        } else {
            where.where['userId'] = req.user.id;
        }

        const include = [
            { model: BeerStyle, attributes: ['name'] }, 
            { model: Brewery }, 
            { model: FileReference},
            { model: User, 
                include: [{model: FileReference, attributes: ['url', 'id']}]
            }
        ];

        const result = await TestedBeer.findAll({
            include,
            ...where,
            ...req.sort,
            ...req.pagination
        });
        const total = await TestedBeer.count({
            include,
            ...where,
            ...req.sort
        });
        res.json({ items: result, totalItems: total });
    } catch (error) {
        res.status(500).send(error.message);
    }
});

router.get('/tested/list', ...listMW, async (req, res) => {
    try {
        const include = [
            { model: BeerStyle, attributes: ['name'] }, 
            { model: Brewery }, 
            { model: FileReference},
            { model: User, 
                include: [{model: FileReference, attributes: ['url', 'id']}]
            }
        ];

        const result = await TestedBeer.findAll({
            include,
            ...req.where,
            ...req.sort,
            ...req.pagination
        });
        const total = await TestedBeer.count({
            include,
            ...req.where,
            ...req.sort
        });
        res.json({ items: result, totalItems: total });
    } catch (error) {
        res.status(500).send(error.message);
    }
});

router.get('/tested/:id', async (req, res) => {
    try {
        const result = await TestedBeer.findByPk(req.params.id, {
            include: [FileReference]
        });
        res.json(result);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

router.delete('/tested/:id', async (req, res) => {
    try {
        const deleted = await TestedBeer.destroy({
            where: { id: req.params.id }
        });

        res.json({ deleted });
    } catch (error) {
        res.status(500).send(error.message);
    }
});

router.post('/tested', async (req, res) => {
    try {
        if (!req.user.id) {
            res.sendStatus(401);
            return;
        }
        const payload = {
            ...req.body,
            userId: req.user.id
        }
        const created = await TestedBeer.create(payload);

        res.json({ created });
    } catch (error) {
        res.status(500).send(error.message);
    }
});

router.patch('/tested/:id', async (req, res) => {
    try {
        const updated = await TestedBeer.update(req.body, { where: { id: req.params.id } });

        res.json({ updated });
    } catch (error) {
        res.status(500).send(error.message);
    }
});

export const BeerController: Router = router;
