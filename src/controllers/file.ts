import { Router } from 'express';
import { upload } from '../utils/multerUpload';
import { saveFile } from '../utils/storageManager';
import { FileReference } from '../models/FileReference';
import axios from 'axios';
const path = require("path");
const fs = require("fs");
const sharp = require('sharp');

const router: Router = Router();

const RESIZED_FOLDER =  path.join(__dirname, '..', '..', 'public', 'testedImages');
const USER_IMAGE_FOLDER =  path.join(__dirname, '..', '..', 'public', 'userImages');

if (!fs.existsSync(RESIZED_FOLDER)) {
    fs.mkdirSync(RESIZED_FOLDER, { recursive: true });
}

if (!fs.existsSync(USER_IMAGE_FOLDER)) {
    fs.mkdirSync(USER_IMAGE_FOLDER, { recursive: true });
}

router.post('/upload/user-image', upload.single('image'), async function (req, res, next) {
    const metadata = await sharp(req.file.path).metadata();
    const {width, height} = metadata;
    const smaller = width < height ? width : height;

    await sharp(req.file.path)
    .extract({
        width: smaller, 
        height: smaller, 
        left: Math.floor((width - smaller) / 2),
        top: Math.floor((height - smaller) / 2)
    })
    .resize(600, 600)
    .toFormat("jpeg", { mozjpeg: true })
    .toFile(path.join(USER_IMAGE_FOLDER, req.file.filename));

    const result = await saveFile({
        filename: req.file.filename, 
        path: path.join(USER_IMAGE_FOLDER, req.file.filename) 
    });

    if (result && result.file) {
        res.json(result.file);
    } else {
        res.status(500).message(res.err);
    }
})

router.post('/uploadTestedImage', upload.single('image'), async function (req, res, next) {
    const metadata = await sharp(req.file.path).metadata();
    const {width, height} = metadata;
    const smaller = width < height ? width : height;

    await sharp(req.file.path)
    .extract({
        width: smaller, 
        height: smaller, 
        left: Math.floor((width - smaller) / 2),
        top: Math.floor((height - smaller) / 2)
    })
    .resize(600, 600)
    .toFormat("jpeg", { mozjpeg: true })
    .toFile(path.join(RESIZED_FOLDER, req.file.filename));

    const result = await saveFile({
        filename: req.file.filename, 
        path: path.join(RESIZED_FOLDER, req.file.filename) 
    });

    if (result && result.file) {
        res.json(result.file);
    } else {
        res.status(500).message(res.err);
    }
})

router.post('/uploadBreweryLogo', upload.single('image'), async function (req, res, next) {
    const metadata = await sharp(req.file.path).metadata();
    const {width, height} = metadata;
    if (width > 500) {
        const newHeigth = Math.round(500 / (width / height));
        await sharp(req.file.path)
        .resize(500, newHeigth)
        .toFormat("jpeg", { mozjpeg: true })
        .toFile(path.join(RESIZED_FOLDER, req.file.filename));
    } else {
        await sharp(req.file.path)
        .toFormat("jpeg", { mozjpeg: true })
        .toFile(path.join(RESIZED_FOLDER, req.file.filename));
    }

    const result = await saveFile({
        filename: req.file.filename, 
        path: path.join(RESIZED_FOLDER, req.file.filename) 
    });

    await fs.promises.unlink(path.join(RESIZED_FOLDER, req.file.filename));
    if (result && result.file) {
        res.json(result.file);
    } else {
        res.status(500).message(res.err);
    }
})

router.get('/private-file/:id', async function (req, res, next) {

    const localFile = await FileReference.findByPk(req.params.id);
    const url = localFile.url;

    const options = {responseType: 'arraybuffer'} as any;
    const response = await axios.get(
        url,
        {...options}
    )
    const buffer = Buffer.from(response.data, "utf-8")
    
    res.send(buffer);

})

export const FileController: Router = router;
