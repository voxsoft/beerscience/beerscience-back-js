import { Router } from 'express';
import { Role, User } from '../models/User';
import { routesAccess } from '../utils/access';
import { FileReference } from '../models/FileReference';
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

const router: Router = Router();
const privateKey = 'supersecret';

const roles = {
  GUEST: 0,
  USER: 1,
  ADMIN: 2
}

export const authJWT = (req, res, next) => {
  const routeAccess = req.method === 'OPTIONS' ? {roles: ['GUEST']} : routesAccess.find(v => 
    req.path.match(v.path) && v.method === req.method.toUpperCase()
  );

  const authHeader = req.headers.authorization;
  if (authHeader) {
    const token = authHeader.split(' ')[1];

    jwt.verify(token, privateKey, (err, user) => {
      if (err || !user) {
        if (routeAccess.roles.indexOf('GUEST') !== -1) {
          return next();
        } else {
          return res.sendStatus(401);
        }
      }
      req.user = user.data;
      if (user && routeAccess.roles.indexOf(req.user.role) !== -1) {
        next();
      } else if (routeAccess.roles.indexOf('GUEST') !== -1) {
        next();
      } else {
        res.sendStatus(401);
      }
    });
  } else {
    if (routeAccess.roles.indexOf('GUEST') !== -1) {
      next();
    } else {
      res.sendStatus(401);
    }
  }
};

router.post('/login', async (req, res) => {
  try {
    if (!req.body.login || !req.body.password) {
      res.status(400).send('Login or password is empty');
      return;
    }
    const user = await User.findOne({ where: { login: req.body.login }, include: [{model: FileReference, attributes: ['url', 'id']}] });
    if (!user) {
      res.status(401).send('No user found');
      return;
    }
    const userJSON = user.toJSON();
    const pass = crypto.createHash('sha256').update(req.body.password).digest('hex');
    if (pass !== user.passHash) {
      res.sendStatus(401);
      return;
    }
    delete userJSON.passHash;
    const token = jwt.sign({
      exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24), // 24 hours
      data: userJSON
    }, privateKey);

    res.json({ token, user: userJSON });
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.post('/logout', async (req, res) => {
  try {
    res.json({});
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.post('/refresh', async (req, res) => {
  try {
    const user = await User.findByPk(req.user.id, {include: [{model: FileReference, attributes: ['url', 'id']}] });
    
    const userJSON = user.toJSON();
    delete userJSON.passHash;
    const token = jwt.sign({
      exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24), // 24 hours
      data: userJSON
    }, privateKey);

    res.json({ token, user: userJSON });
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.post('/signup', async (req, res) => {
  try {
    const hashedPassword = crypto.createHash('sha256').update(req.body.password).digest('hex');
    const newUser = await User.create({
      name: req.body.name,
      login: req.body.login,
      passHash: hashedPassword,
      role: Role.USER
    });

    const userJSON = newUser.toJSON();
    delete userJSON.passHash;
    const token = jwt.sign({
      exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24), // 24 hours
      data: userJSON
    }, privateKey);

    res.json({ token });
  } catch (error) {
    res.status(500).send(error.message);
  }
});

export const AuthController: Router = router;
