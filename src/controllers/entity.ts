import { Router } from 'express';
import { getSequelize } from '../db';
import { Model } from 'sequelize-typescript';
import { authJWT } from './auth';
import { withInclude } from '../middleware/includes';
import { listMW } from '../middleware/list';

type Constructor<T> = new (...args: any[]) => T;
type ModelType<T extends Model<T>> = Constructor<T> & typeof Model;

const router: Router = Router();

const EntityType = (req, res, next) => {
    const modelName = req.params.type;
    const sequelize = getSequelize();
    const model = sequelize.models[modelName];
    if (model) {
        req.model = model;
    }
    next();
}

router.get('/:type/list', EntityType, ...listMW, async (req, res) => {
    try {
        const user = req.user;
        if (req.model) {
            const where = req.where.where ? {...req.where} : {where: {}};
            if (user?.id) {
                where.where['$or'] = [{moderated: true}, {userId: user.id}];
            } else {
                where.where['moderated'] = true;
            }
            const result = await req.model.findAll({
                ...req.include,
                ...where,
                ...req.sort,
                ...req.pagination
            });
            const total = await req.model.count({
                ...req.include,
                ...where,
                ...req.sort
            });
            res.json({items: result, totalItems: total});
        } else {
            res.json([]);
        }
    } catch (error) {
        res.status(500).send(error.message);
    }
});

router.get('/:type/:id', authJWT, withInclude, EntityType, async (req, res) => {
    try {
        const model = req.model as ModelType<any>;
        const result = await model.findByPk(req.params.id, {
            ...req.include
        });
        // if (!req.user || req.user.id !== result.userId) {
        //     res.sendStatus(403);
        // }
        res.json(result);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

router.delete('/:type/:id', EntityType, async (req, res) => {
    try {
        const model = req.model as ModelType<any>;
        const deleted = await model.destroy({
            where: {id: req.params.id}
        });

        res.json({deleted});
    } catch (error) {
        res.status(500).send(error.message);
    }
});

router.post('/:type', EntityType, async (req, res) => {
    try {
        const user = req.user;
        if (!user || !user.id) return res.status(401).send('Not authenticated');
        const model = req.model as ModelType<any>;
        const created = await model.create({...req.body, userId: user.id});

        res.json({created});
    } catch (error) {
        res.status(500).send(error.message);
    }
});

router.patch('/:type/:id', EntityType, async (req, res) => {
    try {
        const model = req.model as ModelType<any>;
        const updated = await model.update(req.body, {where: {id: req.params.id}});

        res.json({updated});
    } catch (error) {
        res.status(500).send(error.message);
    }
});

export const EntityController: Router = router;
