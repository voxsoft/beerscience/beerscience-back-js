export const styles = [
    {
        "name": "Ordinary Bitter",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Gold to copper-colored. Chill haze is allowable at cold temperatures. Malt aroma - Flavor: Low to medium residual malt sweetness should be present. Hop aroma - Flavor: Very low to medium-low. Medium bitterness. Fermentation - Mild carbonation traditionally characterizes draft-cask versions, but in bottled versions, a slight increase in carbon dioxide content is acceptable. Fruity esters are acceptable. Diacetyl is usually absent in these beers but may be present at low levels.. Low to medium body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 35,
        "minABV": 3,
        "maxABV": 4.2
    },
    {
        "name": "Special Bitter or Best Bitter",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Deep gold to deep copper. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Medium residual malt sweetness should be present. Hop aroma - Flavor: Very low to medium at the brewer’s discretion. Medium and not harsh bitterness. Fermentation - Low carbonation traditionally characterizes draft-cask versions, but in bottled versions, a slight increase in carbon dioxide content is acceptable. Fruity esters are acceptable. Diacetyl is usually absent in these beers but may be present at low levels.. Medium body.",
        "moderated": true,
        "minIBU": 28,
        "maxIBU": 40,
        "minABV": 4.2,
        "maxABV": 4.8
    },
    {
        "name": "Extra Special Bitter",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Amber to deep copper. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Medium to medium-high. Hop aroma - Flavor: Medium to medium-high. Medium to medium-high bitterness. Fermentation - Low carbonation traditionally characterizes draft-cask versions, but in bottled versions, a slight increase in carbon dioxide content is acceptable. The overall impression is refreshing and thirst quenching. Fruity esters are acceptable. Diacetyl is usually absent in these beers but may be present at low levels.. Medium body.",
        "moderated": true,
        "minIBU": 30,
        "maxIBU": 45,
        "minABV": 4.8,
        "maxABV": 5.8
    },
    {
        "name": "Scottish-Style Light Ale",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Gold to light brown. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Malty, caramel aroma may be present. A low to medium-low, soft and chewy caramel malt ﬂavor should be present.. Hop aroma - Flavor: Should not be present. Low bitterness. Fermentation - Yeast attributes such as diacetyl and sulfur are acceptable at very low levels. Bottled versions may contain higher amounts of carbon dioxide than is typical for lightly carbonated draft versions. Fruity esters, if present, are low.. Low body.",
        "moderated": true,
        "minIBU": 9,
        "maxIBU": 20,
        "minABV": 2.8,
        "maxABV": 3.5
    },
    {
        "name": "Scottish-Style Heavy Ale",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Amber to dark brown. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Malty, caramel aroma is present. The style exhibits a medium degree of sweet malt and caramel. The overall impression is smooth and balanced.. Hop aroma -  Flavor: Should not be present.  Perceptible but low bitterness. Fermentation - Yeast attributes such as diacetyl and sulfur are acceptable at very low levels. Bottled versions may contain higher amounts of carbon dioxide than is typical for lightly carbonated draft versions. Fruity esters, if present, are low.. Medium, with a soft chewy character body.",
        "moderated": true,
        "minIBU": 12,
        "maxIBU": 20,
        "minABV": 3.5,
        "maxABV": 4.1
    },
    {
        "name": "Scottish-Style Export Ale",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Medium amber to dark chestnut brown. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Sweet malt and caramel aromas and flavors define the character of a Scottish Export. Hop aroma -  Flavor: Should not be present.  Low to medium bitterness. Fermentation - Fruity esters, if present, are low. Yeast attributes such as diacetyl and sulfur are acceptable at very low levels. Bottled versions may contain higher amounts of carbon dioxide than is typical for lightly carbonated draft versions.. Medium body.",
        "moderated": true,
        "minIBU": 15,
        "maxIBU": 25,
        "minABV": 4.1,
        "maxABV": 5.3
    },
    {
        "name": "English-Style Summer Ale",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Straw to gold. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Residual malt sweetness is low to medium. Torriﬁed or malted wheat is often used in quantities of 25 percent or less. Malt attributes such as biscuity or low levels of caramel are present.. Hop aroma -  Flavor: Low to medium, expressed as floral, herbal, earthy, stone fruit, citrus or other attributes. Hop ﬂavor should not be assertive and should be well balanced with malt character..  Medium-low to medium bitterness. Fermentation - Mild carbonation traditionally characterizes draft-cask versions, but in bottled versions, a slight increase in carbon dioxide content is acceptable. Fruity esters are low to medium. Diacetyl and DMS should not be present.. Low to medium-low body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 30,
        "minABV": 3.7,
        "maxABV": 5.1
    },
    {
        "name": "Classic English-Style Pale Ale",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Gold to copper. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Low to medium malt aroma and ﬂavor is present. Low caramel character is allowable.. Hop aroma -  Flavor: Medium-low to medium-high, expressed as ﬂoral, herbal, earthy, stone fruit or other attributes..  Medium-low to medium-high bitterness. Fermentation - Fruity esters are medium to medium-high. Diacetyl is usually absent in these beers but may be present at very low levels.. Medium body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 40,
        "minABV": 4.4,
        "maxABV": 5.3
    },
    {
        "name": "British-Style India Pale Ale",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Gold to copper. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Medium malt ﬂavor should be present. Hop aroma -  Flavor: Medium to high, expressed as ﬂoral, herbal, earthy, stone fruit or other attributes from high hopping rates..  Medium to high bitterness. Fermentation - Fruity esters are medium to high. Traditional interpretations are characterized by medium to medium-high alcohol content. The use of water with high mineral content results in a crisp, dry beer with a subtle and balanced character of sulfur compounds. Diacetyl can be absent or may be present at very low levels.. Medium body.",
        "moderated": true,
        "minIBU": 35,
        "maxIBU": 63,
        "minABV": 4.5,
        "maxABV": 7.1
    },
    {
        "name": "Strong Ale",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Amber to dark brown. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Medium to high malt and caramel sweetness. Very low levels of roast malt may be present.. Hop aroma -  Flavor: Not present to very low.  Present but minimal, and balanced with malt ﬂavors. bitterness. Fermentation - Rich, often sweet and complex fruity ester attributes can contribute to the profile of Strong Ales. Alcohol types can be varied and complex. Diacetyl is usually absent in these beers but may be present at very low levels.. Medium, to full body.",
        "moderated": true,
        "minIBU": 30,
        "maxIBU": 65,
        "minABV": 7,
        "maxABV": 11.3
    },
    {
        "name": "Old Ale",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Copper-red to very dark. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Fruity esters can enhance and complement the malt aroma and ﬂavor proﬁle. Old Ales have malt and sometimes caramel sweetness.. Hop aroma -  Flavor: Very low to medium.  Present but minimal, and balanced with malt flavors. bitterness. Fermentation - Fruity esters can contribute to the character of these beers. Alcohol types can be varied and complex. A distinctive quality of Old Ales is that they undergo an aging process, often for years. Aging can occur on their yeast either in bulk storage or through conditioning in the bottle. This contributes to a rich, wine-like, and often sweet, oxidized character. Complex estery attributes may also emerge. Diacetyl is usually absent in these beers but may be present at very low levels.. Medium, to full body.",
        "moderated": true,
        "minIBU": 30,
        "maxIBU": 65,
        "minABV": 6.3,
        "maxABV": 9.1
    },
    {
        "name": "English-Style Pale Mild Ale",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Light amber to medium amber. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Malt ﬂavor and aroma dominate the ﬂavor proﬁle. Hop aroma -  Flavor: Very low to low.  Very low to low bitterness. Fermentation - Diacetyl is usually absent in these beers but may be present at very low levels. Fruity esters are very low to medium-low.. Low to medium - low body.",
        "moderated": true,
        "minIBU": 10,
        "maxIBU": 20,
        "minABV": 3.4,
        "maxABV": 4.4
    },
    {
        "name": "English-Style Dark Mild Ale",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Reddish-brown to very dark. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Malt attributes such as caramel, licorice, roast or others may be present in aroma and flavor.. Hop aroma -  Flavor: Very low.  Very low to low bitterness. Fermentation - Diacetyl is usually absent in these beers but may be present at very low levels. Fruity esters are very low to medium-low.. Medium-low to medium body.",
        "moderated": true,
        "minIBU": 10,
        "maxIBU": 24,
        "minABV": 3.4,
        "maxABV": 4.4
    },
    {
        "name": "English-Style Brown Ale",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Copper to dark brown. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Roast malt may contribute to a biscuit or toasted aroma proﬁle. Roast malt may contribute to the ﬂavor proﬁle. Malt profile can range from dry to sweet.. Hop aroma -  Flavor: Very low.  Very low to low bitterness. Fermentation - Low to medium-low level fruity esters are appropriate. Diacetyl is usually absent in these beers but may be present at very low levels.. Medium body.",
        "moderated": true,
        "minIBU": 12,
        "maxIBU": 25,
        "minABV": 4.2,
        "maxABV": 6
    },
    {
        "name": "Brown Porter",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Dark brown to very dark. May have red tint.. Beer color may be too dark to perceive clarity. When clarity is perceivable, chill haze is acceptable at low temperatures.. Malt aroma - Flavor: Low to medium malt sweetness. Caramel and chocolate attributes are acceptable. Strong roast barley or strong burnt or black malt character should not be present.. Hop aroma -  Flavor: Very low to medium.  Medium bitterness. Fermentation - Fruity esters are acceptable. Diacetyl is usually absent in these beers but may be present at low levels.. Low to medium body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 30,
        "minABV": 4.4,
        "maxABV": 6
    },
    {
        "name": "Robust Porter",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Very dark brown to black. Opaque. Malt aroma - Flavor: Medium to medium-high. Malty sweetness, roast malt, cocoa and caramel should be in harmony with bitterness from dark malts.. Hop aroma -  Flavor: Very low to medium.  Medium to high bitterness. Fermentation - Fruity esters should be present and balanced with all other characters. Diacetyl should not be present.. Medium, to full body.",
        "moderated": true,
        "minIBU": 25,
        "maxIBU": 40,
        "minABV": 5.1,
        "maxABV": 6.6
    },
    {
        "name": "Sweet Stout or Cream Stout",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Black. Opaque. Malt aroma - Flavor: Medium to medium-high. Malt sweetness, chocolate and caramel should contribute to the aroma and should dominate the ﬂavor proﬁle. Roast ﬂavor may be present. Low to medium-low roasted malt-derived bitterness should be present.. Hop aroma -  Flavor: Should not be present.  Low to medium-low and serves to balance and suppress some of the sweetness without contributing apparent ﬂavor and aroma bitterness. Fermentation - Fruity esters, if present, are low. Diacetyl should not be present.. Full-bodied. body can be increased with the addition of milk sugar (lactose). body.",
        "moderated": true,
        "minIBU": 15,
        "maxIBU": 25,
        "minABV": 3.2,
        "maxABV": 6.3
    },
    {
        "name": "Oatmeal Stout",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Dark brown to black. Beer color may be too dark to perceive. When clarity is perceivable, chill haze is acceptable at low temperatures.. Malt aroma - Flavor: Coffee, caramel, roasted malt, or chocolate aromas should be prominent. Roasted malt character of caramel or chocolate should be smooth without bitterness.. Hop aroma -  Flavor: Optional, but if present should not upset the overall balance..  Medium bitterness. Fermentation - Oatmeal is used in the grist, resulting in a pleasant, full ﬂavor without being grainy. Fruity esters are not present to very low. Diacetyl is usually absent in these beers but may be present at very low levels.. Full with an often-silky mouthfeel body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 40,
        "minABV": 3.8,
        "maxABV": 6.1
    },
    {
        "name": "Scotch Ale or Wee Heavy",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Light reddish-brown to very dark. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Scotch Ales are aggressively malty with a rich and dominant sweet malt aroma and ﬂavor. A caramel character is often part of the proﬁle. Dark roasted malt ﬂavors may be present at low levels.. Hop aroma -  Flavor: Not present to very low.  Not present to very low bitterness. Fermentation - Fruity esters, if present, are generally at low levels. Diacetyl is usually absent in these beers but may be present at low levels.. Full body.",
        "moderated": true,
        "minIBU": 25,
        "maxIBU": 35,
        "minABV": 6.6,
        "maxABV": 8.5
    },
    {
        "name": "British-Style Imperial Stout",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Ranging from dark copper typical of some historic examples, to very dark more typical of contemporary examples. Opaque in darker versions. When clarity is perceivable, chill haze is acceptable at low temperatures.. Malt aroma - Flavor: Extremely rich malty ﬂavor, often expressed as toffee or caramel, and may be accompanied by very low roasted malt astringency.. Hop aroma -  Flavor: Very low to medium, with ﬂoral, citrus or herbal qualities..  Medium and should not overwhelm the overall balance. The bitterness may be higher in darker versions while maintaining balance with sweet malt. bitterness. Fermentation - High alcohol content is evident. Fruity esters if present are medium to high. Diacetyl should not be present.. Full body.",
        "moderated": true,
        "minIBU": 45,
        "maxIBU": 65,
        "minABV": 7,
        "maxABV": 12
    },
    {
        "name": "British-Style Barley Wine Ale",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Tawny copper to deep red/copper-garnet. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Residual malty sweetness is high. Hop aroma -  Flavor: Hop aroma and ﬂavor are very low to medium. English type hops are often used but are not required for this style..  Low to medium bitterness. Fermentation - Complexity of alcohols and fruity ester attributes are often high and balanced with the high alcohol content. Low levels of diacetyl are acceptable. Low carbonation is acceptable in this style.  Caramel and some oxidized character (vinous aromas or ﬂavors) may be considered positive attributes.. Full body.",
        "moderated": true,
        "minIBU": 40,
        "maxIBU": 65,
        "minABV": 8.5,
        "maxABV": 12.2
    },
    {
        "name": "Wood- and Barrel-Aged Beer",
        "typing": [
            "Ale Styles",
            "British Origin Ale Styles"
        ],
        "description": "Color - Varies with underlying style and can be influenced by the color of added fruit(s) if any. Varies with underlying style. Malt aroma - Flavor: Varies with underlying style. Hop aroma -  Flavor: Varies with underlying style.  Varies with underlying style bitterness. Fermentation - Typical of underlying style of beer being aged. Varies with underlying style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Irish-Style Red Ale",
        "typing": [
            "Ale Styles",
            "Irish Origin Ale Styles"
        ],
        "description": "Color - Copper-red to reddish-brown. Chill haze or yeast haze may be present at low levels. Malt aroma - Flavor: Low to medium candy-like caramel malt sweetness should be present in ﬂavor. A toasted malt character should be present, and there may be a slight roast barley or roast malt presence.. Hop aroma -  Flavor: Not present to medium.  Medium bitterness. Fermentation - Low level fruity esters are acceptable. Diacetyl is usually absent in these beers but may be present at very low levels.. Medium body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 28,
        "minABV": 4,
        "maxABV": 4.8
    },
    {
        "name": "Classic Irish-Style Dry Stout",
        "typing": [
            "Ale Styles",
            "Irish Origin Ale Styles"
        ],
        "description": "Color - Black. Opaque. Malt aroma - Flavor: The prominence of coffee-like roasted barley and a moderate degree of roasted malt aroma and flavor deﬁnes much of the character. The hallmark dry-roasted attributes typical of Dry Stout result from the use of roasted barley. Initial malt and light caramel ﬂavors give way to a distinctive dry-roasted bitterness in the ﬁnish.. Hop aroma -  Flavor: European hop character may range from not present to low in aroma and ﬂavor.  Medium to medium-high bitterness. Fermentation - Fruity esters are low relative to malt and roasted barley as well as hop bitterness. Diacetyl is usually absent in these beers but may be present at very low levels. Slight acidity may be present but is not required.. Medium-light to medium body.",
        "moderated": true,
        "minIBU": 30,
        "maxIBU": 40,
        "minABV": 4.1,
        "maxABV": 5.3
    },
    {
        "name": "Export-Style Stout",
        "typing": [
            "Ale Styles",
            "Irish Origin Ale Styles"
        ],
        "description": "Color - Black. Opaque. Malt aroma - Flavor: Coffee-like roasted barley and roasted malt aromas are prominent. Initial malt and light caramel ﬂavors give way to a distinctive dry-roasted bitterness in the ﬁnish.. Hop aroma -  Flavor: Low to medium-low.  May be analytically high, but the perception is lessened by malt sweetness. bitterness. Fermentation - Fruity esters are low. Diacetyl is usually absent in these beers but may be present at very low levels. Slight acidity is acceptable.. Medium, to full body.",
        "moderated": true,
        "minIBU": 30,
        "maxIBU": 60,
        "minABV": 5.6,
        "maxABV": 8
    },
    {
        "name": "Golden or Blonde Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Straw to gold. Chill haze should not be present. Malt aroma - Flavor: Low malt sweetness and toast, cereal-like or other pale malt attributes should be present in ﬂavor and aroma at low to medium-low levels.. Hop aroma -  Flavor: Hop aroma and ﬂavor should be very low to medium, with attributes typical of hops of any origin present but not dominant..  Low to medium bitterness. Fermentation - Fruity esters may be present at low to medium-low levels. Diacetyl and DMS should not be present.. Low to medium, with a crisp finish body.",
        "moderated": true,
        "minIBU": 15,
        "maxIBU": 25,
        "minABV": 4.1,
        "maxABV": 5.1
    },
    {
        "name": "Session India Pale Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Straw to copper. Chill haze is acceptable at low temperatures. Hop haze is allowable at any temperature.. Malt aroma - Flavor: A low to medium maltiness should be present in aroma and ﬂavor.. Hop aroma -  Flavor: Hop aroma and ﬂavor are medium to high, exhibiting a wide range of attributes. Overall hop character is assertive..  Medium to high bitterness. Fermentation - Fruity esters are low to medium. Diacetyl should not be present.. Low to medium body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 55,
        "minABV": 0.5,
        "maxABV": 5
    },
    {
        "name": "American-Style Amber/Red Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Amber to reddish-brown. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Medium-high to high maltiness with low to medium caramel character. Hop aroma -  Flavor: Low to medium-low, exhibiting a wide range of attributes.  Medium to medium-high bitterness. Fermentation - Fruity esters, if present, are low. . Medium, to medium-high body.",
        "moderated": true,
        "minIBU": 25,
        "maxIBU": 45,
        "minABV": 4.4,
        "maxABV": 6.1
    },
    {
        "name": "American-Style Pale Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Straw to light amber. Chill haze is acceptable at low temperatures. Hop haze is allowable at any temperature.. Malt aroma - Flavor: Low caramel malt aroma is allowable. Low to medium maltiness may include low caramel malt character.. Hop aroma -  Flavor: High, exhibiting a wide range of attributes including ﬂoral, citrus, fruity (berry, tropical, stone fruit and other), sulfur, diesel-like, onion-garlic, catty, piney, resinous, and many others..  Medium to medium-high bitterness. Fermentation - Fruity esters may be low to high. Diacetyl should not be present.. Medium body.",
        "moderated": true,
        "minIBU": 30,
        "maxIBU": 50,
        "minABV": 4.4,
        "maxABV": 5.4
    },
    {
        "name": "Juicy or Hazy Pale Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Straw to light amber. Can vary widely from very low haze to very high degree of cloudiness. Starch, yeast, hop, protein or other compounds contribute to a wide range of hazy appearance within this category.. Malt aroma - Flavor: Low to medium-low malt aroma and flavor may be present. Hop aroma -  Flavor: Medium-high to very high hop aroma and flavor are present, exhibiting a very wide range of attributes, especially fruity, tropical, juicy, and many others..  Low to medium. The impression of bitterness is soft and well-integrated into overall balance and may differ significantly from measured or calculated IBU levels. bitterness. Fermentation - Medium-low to medium-high fruity esters are present and can contribute to the perception of sweetness and be complementary to the hop profile. Diacetyl should not be present.. Medium-low to medium-high. Perceived silky or full mouthfeel may contribute to overall flavor profile. body.",
        "moderated": true,
        "minIBU": 5,
        "maxIBU": 30,
        "minABV": 4.4,
        "maxABV": 5.4
    },
    {
        "name": "American-Style Strong Pale Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Pale to copper. Chill haze is acceptable at low temperatures. Hop haze is allowable at any temperature.. Malt aroma - Flavor: Low caramel malt aroma is allowable. Low level maltiness may include low caramel malt character.. Hop aroma -  Flavor: High to very high, exhibiting a wide range of attributes including ﬂoral, citrus, fruity (berry, tropical, stone fruit and other), sulfur, diesel-like, onion-garlic, catty, piney, resinous, and many others..  High bitterness. Fermentation - Fruity esters may be low to high. Diacetyl should not be present.. Medium body.",
        "moderated": true,
        "minIBU": 40,
        "maxIBU": 50,
        "minABV": 5.6,
        "maxABV": 6.4
    },
    {
        "name": "Juicy or Hazy Strong Pale Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Straw to light amber. Can vary widely from very low haze to very high degree of cloudiness. Starch, yeast, hop, protein or other compounds contribute to a wide range of hazy appearance within this category.. Malt aroma - Flavor: Low to medium-low malt aroma and flavor may be present. Hop aroma -  Flavor: Medium-high to very high hop aroma and flavor are present, exhibiting a very wide range of attributes, especially fruity, tropical, juicy, and many others..  Low to medium. The impression of bitterness is soft and well-integrated into overall balance and may differ significantly from measured or calculated IBU levels. bitterness. Fermentation - Medium-low to medium-high fruity esters may be present and can contribute to the perception of sweetness and be complementary to the hop profile. Diacetyl should not be present.. Medium-low to medium-high. A silky or full mouthfeel may contribute to overall flavor profile. body.",
        "moderated": true,
        "minIBU": 15,
        "maxIBU": 40,
        "minABV": 5.6,
        "maxABV": 6.4
    },
    {
        "name": "American-Style India Pale Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Pale to copper. Chill haze is acceptable at low temperatures. Hop haze is allowable at any temperature.. Malt aroma - Flavor: Medium-low to medium intensity malt attributes are present in aroma and ﬂavor. Hop aroma -  Flavor: High to very high, exhibiting a wide range of attributes including ﬂoral, piney, citrus, fruity (berry, tropical, stone fruit and other), sulfur, diesel-like, onion-garlic, catty, resinous, and many others..  Medium-high to very high bitterness. Fermentation - Fruity esters are low to high. Diacetyl and DMS should not be present.. Medium-low to medium body.",
        "moderated": true,
        "minIBU": 50,
        "maxIBU": 70,
        "minABV": 6.3,
        "maxABV": 7.5
    },
    {
        "name": "Juicy or Hazy India Pale Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Straw to light amber. Can vary widely from very low haze to very high degree of cloudiness. Starch, yeast, hop, protein or other compounds contribute to a wide range of hazy appearance within this category.. Malt aroma - Flavor: Low to medium-low malt aroma and flavor may be present. Hop aroma -  Flavor: High to very high hop aroma and flavor are present, exhibiting a very wide range of attributes, especially fruity, tropical, juicy, and many others..  Low to medium. The impression of bitterness is soft and well-integrated into overall balance and may differ significantly from measured or calculated IBU levels. bitterness. Fermentation - Medium to medium-high fruity esters are present, and can contribute to the perception of sweetness and be complementary to the hop profile. Diacetyl should not be present.. Medium-low to medium-high. A silky or full mouthfeel may contribute to overall flavor profile. body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 50,
        "minABV": 6.3,
        "maxABV": 7.5
    },
    {
        "name": "American-Belgo-Style Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Gold to black. Should conform the base beer style. Malt aroma - Flavor: Typically low. Perception of specialty or roasted malts or barley can be very low to robust in darker versions.. Hop aroma -  Flavor: Medium to very high, exhibiting American-type hop aromas not usually found in traditional Belgian styles..  Medium to high, in alignment with base beer style. bitterness. Fermentation - Fruity esters are medium to high. Belgian yeast attributes such as banana, berry, apple, coriander, spice or smoky-phenolic should be in balance with malt and hops. Diacetyl, sulfur, and attributes typical of <i>Brettanomyces</i> should not be present.. Medium-low to medium, in alignment with base beer style. body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "American-Style Brown Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Deep copper to very dark brown. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Medium levels of roasted malt, caramel, and chocolate aromas and flavors should be present.. Hop aroma -  Flavor: Medium-low to medium-high.  Medium to high bitterness. Fermentation - Low to medium-low fruity esters may be present. Diacetyl should not be present.. Medium body.",
        "moderated": true,
        "minIBU": 30,
        "maxIBU": 45,
        "minABV": 4.2,
        "maxABV": 6.3
    },
    {
        "name": "American-Style Black Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Very dark to black. Opaque. Malt aroma - Flavor: Low to medium-low caramel malt and dark roasted malt aromas and ﬂavors are present. Astringency and burnt character of roast malt should be absent.. Hop aroma -  Flavor: Medium-high to high, with fruity, citrusy, piney, ﬂoral, herbal or other aromas derived from hops of all origins..  Medium-high to high bitterness. Fermentation - Fruity esters are low to medium. Diacetyl should not be present.. Medium body.",
        "moderated": true,
        "minIBU": 40,
        "maxIBU": 70,
        "minABV": 6.3,
        "maxABV": 7.6
    },
    {
        "name": "American-Style Stout",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Black. Opaque. Malt aroma - Flavor: Coffee-like roasted barley and roasted malt aromas are prominent. Low to medium malt sweetness with any of caramel, chocolate, or roasted coffee attributes present at low to medium levels, with a distinct dry-roasted bitterness in the ﬁnish. Astringency from roasted malt and roasted barley is low. Slight roasted malt acidity is acceptable.. Hop aroma -  Flavor: Medium to high, often with any of citrusy, resiny or other attributes typical of many American hop varieties..  Medium to high bitterness. Fermentation - Fruity esters are low. Diacetyl is usually absent in these beers but may be present at very low levels.. Medium, to full body.",
        "moderated": true,
        "minIBU": 35,
        "maxIBU": 60,
        "minABV": 5.7,
        "maxABV": 8
    },
    {
        "name": "American-Style Imperial Porter",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Black. Opaque. Malt aroma - Flavor: No roast barley or strong burnt/black malt character should be present. Medium malt, caramel, and cocoa sweetness should be present.. Hop aroma -  Flavor: Low to medium-high.  Medium-low to medium bitterness. Fermentation - Fruity esters are present but not overpowering and should complement hop character and malt-derived sweetness. No diacetyl should not be present.. Full body.",
        "moderated": true,
        "minIBU": 35,
        "maxIBU": 50,
        "minABV": 7,
        "maxABV": 12
    },
    {
        "name": "American-Style Imperial Stout",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Black. Opaque. Malt aroma - Flavor: Extremely rich malty aroma is typical. Extremely rich malty ﬂavor with full sweet malt character is typical. Roasted malt astringency and bitterness can be moderate but should not dominate the overall character.. Hop aroma -  Flavor: Medium-high to high, exhibiting any of ﬂoral, citrus, herbal, or any other attributes typical of American hops..  Medium-high to very high and balanced with rich malt character. bitterness. Fermentation - Fruity esters are high. Diacetyl should not be present.. Full body.",
        "moderated": true,
        "minIBU": 50,
        "maxIBU": 80,
        "minABV": 7,
        "maxABV": 12
    },
    {
        "name": "Double Hoppy Red Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Deep amber to dark copper/reddish-brown. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Medium to medium-high caramel malt character should be present in ﬂavor and aroma. Low to medium biscuit or toasted malt character may also be present.. Hop aroma -  Flavor: Hop aroma is high, derived from any variety of hops. Hop ﬂavor is high and balanced with other beer attributes..  High to very high bitterness. Fermentation - Alcohol content is medium to high. Complex alcohol ﬂavors may be present. Fruity esters are medium. Diacetyl should not be present.. Medium, to full body.",
        "moderated": true,
        "minIBU": 45,
        "maxIBU": 80,
        "minABV": 6.1,
        "maxABV": 7.9
    },
    {
        "name": "Imperial Red Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Deep amber to dark copper/reddish-brown. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Medium to high caramel malt character is present in aroma and ﬂavor. Hop aroma -  Flavor: High, derived from any variety of hops. Hop ﬂavor is prominent and balanced with other beer attributes..  Very high bitterness. Fermentation - Very high alcohol is a hallmark of this style. Complex alcohol ﬂavors may be present. Fruity esters are medium. Diacetyl should not be present.. Full body.",
        "moderated": true,
        "minIBU": 55,
        "maxIBU": 85,
        "minABV": 8,
        "maxABV": 10.6
    },
    {
        "name": "American-Style Imperial or Double India Pale Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Straw to medium amber. Chill haze is acceptable at low temperatures. Haze created by dry hopping is allowable at any temperature.. Malt aroma - Flavor: Low to medium pale malt character is typical. Low pale caramel malt character may be present.. Hop aroma -  Flavor: High to intense, exhibiting a wide range of attributes including ﬂoral, piney, citrus, fruity (berry, tropical, stone fruit, and other), sulfur, diesel-like, onion-garlic, catty, resinous, and many others. Hop character should be fresh and evident, and should not be harsh..  Very high but not harsh bitterness. Fermentation - Alcohol content is medium-high to high and evident. Fruity esters are medium to high. Diacetyl should not be present.. Medium, to full body.",
        "moderated": true,
        "minIBU": 65,
        "maxIBU": 100,
        "minABV": 7.6,
        "maxABV": 10.6
    },
    {
        "name": "Juicy or Hazy Imperial or Double India Pale Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Straw to light amber. Can vary widely from very low haze to very high degree of cloudiness. Starch, yeast, hop, protein or other compounds contribute to a wide range of hazy appearance within this category.. Malt aroma - Flavor: Low to medium malt aroma and flavor may be present. Hop aroma -  Flavor: High to intense, exhibiting a very wide range of attributes, especially fruity, tropical, juicy, and many others..  Low to medium. The impression of bitterness is soft and well-integrated into overall balance, and may differ significantly from measured or calculated IBU levels. bitterness. Fermentation - Medium-high to high fruity esters are present, and can contribute to the perception of sweetness and be complementary to the hop profile. Diacetyl should not be present.. Medium, to high. A silky or full mouthfeel may contribute to overall flavor profile. body.",
        "moderated": true,
        "minIBU": 30,
        "maxIBU": 80,
        "minABV": 7.6,
        "maxABV": 10.6
    },
    {
        "name": "American-Style Barley Wine Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Amber to deep red/copper-garnet. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Caramel or toffee malt aroma attributes are often present. High residual malty sweetness, often exhibiting caramel or toffee attributes, should be present.. Hop aroma -  Flavor: Medium to very high, exhibiting a wide range of attributes..  High bitterness. Fermentation - Complex alcohols are evident. Fruity esters are often high. Diacetyl is usually absent in these beers but may be present at very low levels.. Full body.",
        "moderated": true,
        "minIBU": 60,
        "maxIBU": 100,
        "minABV": 8.5,
        "maxABV": 12.2
    },
    {
        "name": "American-Style Wheat Wine Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Gold to black. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Any of bready, wheat, honey or caramel malt aroma and flavor attributes are often present. High residual malt sweetness should be present.. Hop aroma -  Flavor: Low to medium.  Medium to medium-high bitterness. Fermentation - Fruity esters are often high and balanced by a complexity of alcohols and high alcohol content. Diacetyl is usually absent in these beers but may be present at very low levels. Phenolic yeast character, sulfur, and DMS should not be present. Oxidized, stale, and aged attributes are not typical of this style.. Full body.",
        "moderated": true,
        "minIBU": 45,
        "maxIBU": 85,
        "minABV": 8.5,
        "maxABV": 12.2
    },
    {
        "name": "Smoke Porter",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Dark brown to black. Opaque. Malt aroma - Flavor: Smoked porters will exhibit mild to assertive smoke malt aroma and flavor in balance with other aroma attributes. Black malt character can be present in some porters, while others may be absent of strong roast character. Roast barley character is absent to low depending on underlying porter style being smoked. Medium to high malt sweetness with caramel or chocolate flavor attributes is acceptable.. Hop aroma -  Flavor: None to medium.  Medium to medium-high bitterness. Fermentation - Low to medium fruity esters are acceptable. Medium, to full body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "American-Style Sour Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Pale to black. Chill haze, bacteria, and yeast-induced haze is acceptable at any temperature.. Malt aroma - Flavor: Low. In darker versions, any of roasted malt, caramel, or chocolate aroma and flavor attributes should be present at low levels.. Hop aroma -  Flavor: None to high.  None to high bitterness. Fermentation - Moderate to intense, yet balanced, fruity esters are present. Diacetyl, DMS, and Brettanomyces should not be present. The evolution of natural acidity at medium-low to high levels develops a balanced complexity and is expressed as a refreshing, pleasant sourness, in harmony with other attributes. The acidity present is usually in the form of lactic, acetic, and other organic acids naturally developed with acidiﬁed malt in the mash or in kettle or post wort fermentation and is produced by various microorganisms including certain bacteria and yeasts. Acidic character can be a complex balance of several types of acid and attributes of age. Wood vessels may be used during the fermentation and aging process, but wood-derived ﬂavors such as vanillin should not be present. There should be no residual ﬂavors from liquids previously aged in a barrel such as bourbon or sherry.. Low to high body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "American-Style Fruited Sour Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Can range from pale to black depending on underlying beer style and is often influenced by the color of added fruit. Chill haze, bacteria, and yeast-induced haze is acceptable at any temperature.. Malt aroma - Flavor: Low. In darker versions, any of roasted malt, caramel, or chocolate aroma and flavor attributes should be present at low levels.. Hop aroma -  Flavor: None to high.  None to high and in balance with fruit character bitterness. Fermentation - Moderate to intense, yet balanced, fruity esters are present. Diacetyl, DMS, and <i>Brettanomyces</i> should not be present. The evolution of natural acidity at medium-low to high levels develops a balanced complexity and is expressed as a refreshing, pleasant sourness, in harmony with other attributes. The acidity present is usually in the form of lactic, acetic, and other organic acids naturally developed with acidiﬁed malt in the mash or in kettle or post wort fermentation and is produced by various microorganisms, including certain bacteria and yeasts. Acidic character can be a complex balance of several types of acid and attributes of age. Wood vessels may be used during the fermentation and aging process, but wood-derived ﬂavors such as vanillin should not be present. Attributes arising from liquids previously aged in a barrel, such as bourbon or sherry, should not be present.. Low to high body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "West Coast-Style India Pale Ale",
        "typing": [
            "Ale Styles",
            "North American Origin Ale Styles"
        ],
        "description": "Color - Straw to gold. Chill haze or hop haze is acceptable at low levels. Malt aroma - Flavor: Low to medium-low. Caramel or roasted malt character should not be present. Hop aroma -  Flavor: High to very high, exhibiting a wide range of attributes including floral, piney, citrus, fruity (berry, tropical, stone fruit and other), sulfur, diesel-like, onion-garlic, catty, resinous and many others..  Medium-high to very high, but not harsh bitterness. Fermentation - Fruity esters range from low to medium. DMS, acetaldehyde, and diacetyl should not be present. These beers are characterized by a high degree of attenuation.. Low to medium body.",
        "moderated": true,
        "minIBU": 50,
        "maxIBU": 75,
        "minABV": 6.3,
        "maxABV": 7.5
    },
    {
        "name": "German-Style Koelsch",
        "typing": [
            "Ale Styles",
            "German Origin Ale Styles"
        ],
        "description": "Color - Straw to gold. Chill haze should not be present. Malt aroma - Flavor: Malt character is very low to low with soft sweetness. Caramel character should not be present.. Hop aroma -  Flavor: Low and, if present, should express noble hop character..  Medium to medium-high bitterness. Fermentation - Fruity esters are absent to low, expressed as pear, apple, or wine-like attributes when present. Diacetyl should not be present.. Low to medium - low. Dry and crisp. body.",
        "moderated": true,
        "minIBU": 22,
        "maxIBU": 30,
        "minABV": 4.8,
        "maxABV": 5.3
    },
    {
        "name": "German-Style Altbier",
        "typing": [
            "Ale Styles",
            "German Origin Ale Styles"
        ],
        "description": "Color - Copper to dark brown. Clear to slightly hazy. Chill haze should not be present. Malt aroma - Flavor: A variety of malts contributes to medium-low to medium malt aroma and flavor. Toast aroma typical of Munich malts should be present. Slight nuttiness is acceptable. Roast malt character should be present at low levels and well-integrated with the overall malt profile. Smoke character should not be present.. Hop aroma -  Flavor: Low to medium with hop flavor more perceptible than aroma, with attributes typical of traditional German noble hops..  Medium to high, producing a clean dry finish. Forty-plus IBU is typical for Altbiers originating in Dusseldorf. bitterness. Fermentation - Fruity esters are absent to low, with attributes expressed subtly as citrus, pear, dark cherry or plum. A slight sulfur aroma is acceptable. Diacetyl should not be present.. Medium-low to medium. body.",
        "moderated": true,
        "minIBU": 25,
        "maxIBU": 52,
        "minABV": 4.6,
        "maxABV": 5.6
    },
    {
        "name": "Berliner-Style Weisse",
        "typing": [
            "Ale Styles",
            "German Origin Ale Styles"
        ],
        "description": "Color - Straw to pale. These are the lightest of all the German wheat beers.. May appear hazy or cloudy from yeast or chill haze. Malt aroma - Flavor: Malt sweetness is absent. Hop aroma -  Flavor: Not present.  Not present to very low bitterness. Fermentation - Fruity esters are low to medium. Diacetyl should not be present. <i>Brettanomyces</i> character may be absent or present at low to medium levels and, if present, may be expressed as any of horsey, goaty, leathery, phenolic, fruity, or acidic aroma and flavor attributes. The unique combination of yeast and lactic acid bacteria fermentation yields a beer that is acidic and highly attenuated.. Very low body.",
        "moderated": true,
        "minIBU": 3,
        "maxIBU": 6,
        "minABV": 2.8,
        "maxABV": 5
    },
    {
        "name": "Leipzig-Style Gose",
        "typing": [
            "Ale Styles",
            "German Origin Ale Styles"
        ],
        "description": "Color - Straw to light amber. Clear to hazy. Haze may or may not be from yeast.. Malt aroma - Flavor: Malt sweetness and attributes are not present to very low. Hop aroma -  Flavor: Not present.  Not present to low bitterness. Fermentation - Medium to high lactic acid character should be present and expressed as a sharp, refreshing sourness. These beers are not excessively aged.. Low to medium - low body.",
        "moderated": true,
        "minIBU": 5,
        "maxIBU": 15,
        "minABV": 4.4,
        "maxABV": 5.4
    },
    {
        "name": "Contemporary-Style Gose",
        "typing": [
            "Ale Styles",
            "German Origin Ale Styles"
        ],
        "description": "Color - Usually straw to medium amber and can take on the color of added fruits or other ingredients such as darker malts.. Clear to hazy. Haze may or may not result from yeast. Malt aroma - Flavor: Malt aroma and flavor is not present to very low. Hop aroma -  Flavor: Very low to low.  Not present to medium bitterness. Fermentation - Horsey, leathery, or earthy aromas contributed by Brettanomyces yeasts may be present but at low levels as these beers do not undergo prolonged aging. Contemporary Gose may be fermented with pure beer yeast strains, or with yeast mixed with bacteria. Alternatively, they may be spontaneously fermented. Low to medium lactic acid character is present in all examples expressed as a sharp, refreshing sourness.. Low to medium - low body.",
        "moderated": true,
        "minIBU": 5,
        "maxIBU": 30,
        "minABV": 4.4,
        "maxABV": 5.4
    },
    {
        "name": "South German-Style Hefeweizen",
        "typing": [
            "Ale Styles",
            "German Origin Ale Styles"
        ],
        "description": "Color - Straw to amber. If served with yeast, appearance may be very cloudy.. Malt aroma - Flavor: Very low to medium-low. Hop aroma -  Flavor: Not present to very low.  Very low bitterness. Fermentation - Medium-low to medium-high fruity and phenolic attributes are hallmarks of this style. Phenolic attributes such as clove, nutmeg, smoke, and vanilla are present. Banana ester aroma and ﬂavor should be present at low to medium-high levels. Diacetyl should not be present.. Medium, to full body.",
        "moderated": true,
        "minIBU": 10,
        "maxIBU": 15,
        "minABV": 4.9,
        "maxABV": 5.6
    },
    {
        "name": "South German-Style Kristal Weizen",
        "typing": [
            "Ale Styles",
            "German Origin Ale Styles"
        ],
        "description": "Color - Straw to amber. Clear with no chill haze present. Because the beer is ﬁltered, no yeast should be present.. Malt aroma - Flavor: Malt sweetness is very low to medium-low. Hop aroma -  Flavor: Not present to very low.  Very low bitterness. Fermentation - The aroma and flavor are very similar to Hefeweizen with the caveat that fruity and phenolic characters are not combined with the yeasty ﬂavor and fuller-bodied mouthfeel of yeast. The phenolic characteristics are often described as clove-like or nutmeg-like and can be smoky or even vanilla-like. A Banana-like ester aroma and flavor is often present. Diacetyl should not be present. Kristal Weizen is well attenuated and very highly carbonated.. Medium, to full body.",
        "moderated": true,
        "minIBU": 10,
        "maxIBU": 15,
        "minABV": 4.9,
        "maxABV": 5.6
    },
    {
        "name": "German-Style Leichtes Weizen",
        "typing": [
            "Ale Styles",
            "German Origin Ale Styles"
        ],
        "description": "Color - Straw to copper-amber. If served with yeast, appearance may be very cloudy.. Malt aroma - Flavor: Very low to medium-low. Hop aroma -  Flavor: Not present to very low.  Very low bitterness. Fermentation - The phenolic and estery aromas typical of Weissbiers should be present but less pronounced in this style. The overall ﬂavor proﬁle is less complex than Hefeweizen due to a lower alcohol content and there is less yeasty ﬂavor. Diacetyl should not be present.. Low with a lighter mouthfeel than Hefeweizen.The German word ‘leicht’ means light, and as such these beers are light versions of Hefeweizen. body.",
        "moderated": true,
        "minIBU": 10,
        "maxIBU": 15,
        "minABV": 2.5,
        "maxABV": 3.5
    },
    {
        "name": "South German-Style Bernsteinfarbenes Weizen",
        "typing": [
            "Ale Styles",
            "German Origin Ale Styles"
        ],
        "description": "Color - Amber to light brown. The German word ‘Bernsteinfarben’ means amber colored.. If served with yeast, appearance may be very cloudy.. Malt aroma - Flavor: Distinct sweet maltiness and caramel or bread-like character arises from the use of medium-colored malts.. Hop aroma -  Flavor: Not present.  Low bitterness. Fermentation - The phenolic and estery aromas and flavors typical of Weissbiers are present but less pronounced in Bernsteinfarbenes Weissbiers. These beers should be well attenuated and very highly carbonated. Diacetyl should not be present.. Medium, to full body.",
        "moderated": true,
        "minIBU": 10,
        "maxIBU": 15,
        "minABV": 4.8,
        "maxABV": 5.4
    },
    {
        "name": "South German-Style Dunkel Weizen",
        "typing": [
            "Ale Styles",
            "German Origin Ale Styles"
        ],
        "description": "Color - Copper-brown to very dark. If served with yeast, appearance may be very cloudy. Malt aroma - Flavor: Distinct sweet maltiness and a chocolate-like character from dark or roasted malts exemplify beer in this style. Dark barley malts are frequently used along with dark Cara or color malts.. Hop aroma -  Flavor: Not present.  Low bitterness. Fermentation - The phenolic and estery aromas and flavors typical of Weissbiers are present but less pronounced in Dunkel Weissbiers. Dunkel Weissbiers should be well attenuated and very highly carbonated. Diacetyl should not be present. Medium, to full body.",
        "moderated": true,
        "minIBU": 10,
        "maxIBU": 15,
        "minABV": 4.8,
        "maxABV": 5.4
    },
    {
        "name": "South German-Style Weizenbock",
        "typing": [
            "Ale Styles",
            "German Origin Ale Styles"
        ],
        "description": "Color - Gold to very dark. If served with yeast, appearance may be very cloudy.. Malt aroma - Flavor: Medium malty sweetness should be present. If dark, a mild roast malt character should emerge in the flavor and, to a lesser degree, in the aroma.. Hop aroma -  Flavor: Not present.  Low bitterness. Fermentation - Balanced, clove-like phenolic and fruity ester banana notes produce a well-rounded ﬂavor and aroma. Diacetyl should not be present. Carbonation should be high.. Medium, to full body.",
        "moderated": true,
        "minIBU": 15,
        "maxIBU": 35,
        "minABV": 7,
        "maxABV": 9.5
    },
    {
        "name": "German-Style Rye Ale",
        "typing": [
            "Ale Styles",
            "German Origin Ale Styles"
        ],
        "description": "Color - Pale to very dark, with darker versions ranging from dark amber to dark brown.. Chill haze is acceptable in versions packaged and served without yeast. In versions served with yeast, appearance may range from hazy to very cloudy.. Malt aroma - Flavor: In darker versions, malt aromas and flavors can optionally include low level roasted malt characters expressed as cocoa-chocolate, caramel, toffee, or biscuit attributes. Malt sweetness can vary from low to medium. Low level of roast malt astringency is acceptable when balanced with low to medium malt sweetness.. Hop aroma -  Flavor: Not present.  Very low to low bitterness. Fermentation - Low to medium banana–like or other fruity ester aromas and flavors are typical. Clove-like or other phenolic aromas and flavors should also be present. No yeast aroma should be present in versions without yeast. Versions packaged and served without yeast will not have yeast ﬂavor or full mouthfeel typical of beers with yeast. Versions with yeast will have low to medium yeast aroma and ﬂavor and a full mouthfeel, but the yeast character should not overpower the balance of rye and barley malts, esters, and phenolics.. Low to medium body.",
        "moderated": true,
        "minIBU": 10,
        "maxIBU": 15,
        "minABV": 4.9,
        "maxABV": 5.6
    },
    {
        "name": "Bamberg-Style Weiss Rauchbier",
        "typing": [
            "Ale Styles",
            "German Origin Ale Styles"
        ],
        "description": "Color - Pale to chestnut brown. If served with yeast, appearance may be very cloudy.. Malt aroma - Flavor: In darker versions, a detectable degree of roast malt may be present without being aggressive. Smoky malt aroma and ﬂavor, ranging from low to high, should be present. Smoke character should be smooth, not harshly phenolic, suggesting a mild sweetness.. Hop aroma -  Flavor: Not present.  Low bitterness. Fermentation - The aroma and flavor of a Weiss Rauchbier with yeast should be fruity and phenolic. The phenolic characteristics are often described as clove, nutmeg, vanilla, and smoke. Banana esters are often present at low to medium-high levels. No diacetyl should be perceived. Weissbiers are well attenuated and very highly carbonated.. Medium, to full body.",
        "moderated": true,
        "minIBU": 10,
        "maxIBU": 15,
        "minABV": 4.9,
        "maxABV": 5.6
    },
    {
        "name": "Kellerbier or Zwickelbier",
        "typing": [
            "Ale Styles",
            "German Origin Ale Styles"
        ],
        "description": "Color - Varies depending on the underlying European origin lager or ale style. Typically slightly hazy to moderately cloudy, but may become clear through settling. A small amount of yeast haze is acceptable and traditional.. Malt aroma - Flavor: Varies depending on the underlying style. Hop aroma -  Flavor: Varies depending on underlying style. Low level attributes typical of late or dry hopping may be present in some versions..  Varies depending on underlying style bitterness. Fermentation - Low levels of sulfur and acetaldehyde or other volatiles normally scrubbed during fermentation, if present, can enhance the flavor of these beers. Low fruity esters may be present and may vary slightly from the underlying style due to age and the presence of yeast. Diacetyl is usually absent in these beers but may be present at low levels in keller versions of beer styles which can contain diacetyl when fully aged, such as Bohemian-Style Lager.. Varies depending on underlying style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Belgian-Style Table Beer",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Gold to black. Caramel color is sometimes added to adjust color.. Beer color may be too dark to perceive. When clarity is perceivable, chill haze is acceptable at low temperatures.. Malt aroma - Flavor: Mild malt character may be present. Hop aroma -  Flavor: Not present to very low.  Very low to low bitterness. Fermentation - Diacetyl should not be present. Traditional versions do not use artiﬁcial sweeteners nor are they excessively sweet. More modern versions can incorporate sweeteners such as sugar and saccharin added post fermentation for additional sweetness and to increase smoothness.. Low body.",
        "moderated": true,
        "minIBU": 5,
        "maxIBU": 15,
        "minABV": 0.5,
        "maxABV": 2
    },
    {
        "name": "Belgian-Style Session Ale",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - May vary widely. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Very low to low, exhibiting a wide range of malt-derived attributes. Hop aroma -  Flavor: Very low to low, exhibiting a wide range of hop-derived attributes.  Very low to low but sufficient to balance other attributes bitterness. Fermentation - Phenolic spiciness may be absent or may be present at low levels. Fruity-ester complexity may range from low to medium, in harmony with malt and other attributes. Diacetyl should not be present.. Very low to low body.",
        "moderated": true,
        "minIBU": 5,
        "maxIBU": 35,
        "minABV": 2.1,
        "maxABV": 5
    },
    {
        "name": "Belgian-Style Speciale Belge",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Gold to light copper. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Malt aroma should be low. Caramel or toasted malt attributes are acceptable.. Hop aroma -  Flavor: Very low to medium. Noble-type hops are commonly used..  Low to medium bitterness. Fermentation - Low to medium fruity esters are present. Yeast-derived phenolic spicy ﬂavors and aromas should be present at low to medium-low levels. Diacetyl should not be present.. Low to medium body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 30,
        "minABV": 5.1,
        "maxABV": 6.3
    },
    {
        "name": "Belgian-Style Blonde Ale",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Straw to light amber. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Very low to low. Hop aroma -  Flavor: Very low to medium. Noble-type hops are commonly used..  Very low to medium-low bitterness. Fermentation - Low to medium fruity esters are balanced with low level malt attributes. Low level yeast-derived phenolic spiciness may be present. Diacetyl and acidic character should not be present.. Low to medium body.",
        "moderated": true,
        "minIBU": 15,
        "maxIBU": 40,
        "minABV": 6.3,
        "maxABV": 7.9
    },
    {
        "name": "Belgian-Style Strong Blonde Ale",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Straw to light amber. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Malt character is low to medium. A complex fruitiness is often present.. Hop aroma -  Flavor: Medium-low to medium-high.  Medium-low to medium-high bitterness. Fermentation - Low to medium fruity esters are present. Yeast-derived phenolic spicy ﬂavors and aromas should be present at low to medium-low levels. Diacetyl is usually absent in these beers but may be present at very low levels.. Very low to medium body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 50,
        "minABV": 7.1,
        "maxABV": 11.2
    },
    {
        "name": "Belgian-Style Strong Dark Ale",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Amber to very dark. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Medium to high malt aroma and complex fruity aromas are distinctive. Medium to high malt intensity can be rich, creamy, and sweet. Fruity complexity along with soft roasted malt ﬂavor adds distinct character.. Hop aroma -  Flavor: Low to medium.  Low to medium bitterness. Fermentation - Yeast-derived phenolic spicy ﬂavors and aromas are present at low to medium-low levels. Diacetyl is usually absent in these beers but may be present at very low levels. . Medium, to full body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 50,
        "minABV": 7.1,
        "maxABV": 11.2
    },
    {
        "name": "Belgian-Style Dubbel",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Brown to very dark. Chill haze is acceptable at low temperatures. Slight yeast haze may be present in bottle conditioned versions.. Malt aroma - Flavor: Any of cocoa, dark or dried fruit, or caramel aroma attributes should be present along with malty sweetness.. Hop aroma -  Flavor: Absent, or low if present..  Medium-low to medium bitterness. Fermentation - Fruity esters (especially banana) are absent or present at low levels. Clove-like phenolic flavor and aroma may be present at low to medium-low levels. Diacetyl character should not be present.. Low to medium body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 35,
        "minABV": 6.3,
        "maxABV": 7.6
    },
    {
        "name": "Belgian-Style Tripel",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Pale to pale gold. Chill haze is acceptable at low temperatures. Traditional Tripels are bottle conditioned and may exhibit slight yeast haze. However, yeast should not be intentionally roused.. Malt aroma - Flavor: Low sweetness from very pale malts should be present. There should be no roasted or dark malt character.. Hop aroma -  Flavor: Absent, or low if present..  Medium to medium-high bitterness. Fermentation - A complex, sometimes mildly spicy, aroma and ﬂavor characterize this style. Clove-like phenolic aroma and ﬂavor may be very low. Fruity esters, including banana, are also common, but not required. Traditional Tripels are often well attenuated. Alcohol strength and ﬂavor should be present.. Medium body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 45,
        "minABV": 7.1,
        "maxABV": 10.1
    },
    {
        "name": "Belgian-Style Quadrupel",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Amber to dark brown. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Caramel, dark sugar and malty sweet ﬂavors and aromas can be intense, but not cloying, and should complement fruitiness.. Hop aroma -  Flavor: Not present to very low.  Low to medium-low bitterness. Fermentation - Perception of alcohol can be strong. Complex fruity attributes reminiscent of any of raisins, dates, ﬁgs, grapes, or plums are often present and may be accompanied by wine-like attributes at low levels. Clove-like phenolic ﬂavor and aroma may be present at low to medium-low levels. Diacetyl and DMS should not be present.. Full with creamy mouthfeel body.",
        "moderated": true,
        "minIBU": 25,
        "maxIBU": 50,
        "minABV": 10,
        "maxABV": 14.2
    },
    {
        "name": "Belgian-Style Witbier",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Straw to pale. Unﬁltered starch and yeast haze should be visible. Wits are traditionally bottle conditioned and served cloudy.. Malt aroma - Flavor: Very low to low. Hop aroma -  Flavor: Hop aroma is not present to low. Hop flavor is not present..  Low, from noble-type hops. bitterness. Fermentation - Low to medium fruity esters are present. Mild phenolic spiciness and yeast ﬂavors may be present. Mild acidity is appropriate. Diacetyl should not be present. . Low to medium, with a degree of creaminess from wheat starch. body.",
        "moderated": true,
        "minIBU": 10,
        "maxIBU": 17,
        "minABV": 4.8,
        "maxABV": 5.6
    },
    {
        "name": "Classic French Belgian-Style Saison",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Straw to light amber. Chill haze or slight yeast haze is acceptable. Malt aroma - Flavor: Low, but providing foundation for the overall balance.. Hop aroma -  Flavor: Low to medium and characterized by any of floral, herbal, woody or other attributes typical of European-type hops are common..  Medium-low to medium, but not assertive. bitterness. Fermentation - Fruity esters are medium to high. Low to medium-low level phenolics may be present, expressed as spice-like or other attributes. Phenolics should not be harsh or dominant and should be in harmony with ester profile and hops. Fruity and spicy black pepper attributes derived from Belgian yeast are common. Diacetyl should not be present. Low levels of <i>Brettanomyces</i> yeast-derived aroma and flavor attributes including any of slightly acidic, fruity, horsey, goaty, or leather-like, may be present but are not required. These beers are well attenuated and often bottle conditioned contributing some yeast character and high carbonation. Versions which exhibit sensory attributes typical of wood-aging are characterized as Specialty Saison.. Very low to low body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 38,
        "minABV": 5,
        "maxABV": 6.8
    },
    {
        "name": "Specialty Saison",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Straw to dark brown; may take on hue of fruit(s), darker malts, or other ingredients. Chill haze or slight yeast haze is acceptable. Malt aroma - Flavor: Typically low to medium-low, but may vary in beers made with specialty malts.. Hop aroma -  Flavor: Low to medium-high.  Medium to medium-high bitterness. Fermentation - Fruity esters are medium to high. Diacetyl should not be present. Complex alcohols, herbs, spices, low <i>Brettanomyces</i> attributes including slightly acidic, fruity, horsey, goaty and leather-like, as well as clove-like and smoky phenolics may be present. Herb or spice ﬂavors, including notes of black pepper, may be present. A low level of sour acidic ﬂavor is acceptable when in balance with other components. Because these beers are often bottle conditioned, they may display some yeast character and high carbonation.. Low to medium body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 40,
        "minABV": 5,
        "maxABV": 9.3
    },
    {
        "name": "French-Style Bière de Garde",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Light amber to chestnut brown/red. Chill haze is acceptable. These beers are often bottle conditioned so slight yeast haze is acceptable.. Malt aroma - Flavor: These beers are characterized by a toasted malt aroma and flavor, and a slight malt sweetness.. Hop aroma -  Flavor: Low to medium from noble-type hops.  Low to medium bitterness. Fermentation - Fruity ester aromas are medium to high. Whereas fruity ester ﬂavors are low to medium. Diacetyl should not be present. Bière de Garde may have low levels of <i>Brettanomyces</i> yeast-derived ﬂavors including any of slightly acidic, fruity, horsey, goaty, or leather-like attributes. Beer displaying more pronounced levels of <i>Brettanomyces</i> derived attributes is categorized as Brett Beer. Alcohol may be evident in higher strength beers.. Low to medium body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 30,
        "minABV": 4.4,
        "maxABV": 8
    },
    {
        "name": "Belgian-Style Flanders Oud Bruin or Oud Red Ale",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Copper to very dark. SRM/EBC color values can be misleading because the red spectrum of color is not accurately assessed by these measurement systems.. Chill haze is acceptable at low temperatures. Some versions may be more highly carbonated. Bottle conditioned versions may appear cloudy when served.. Malt aroma - Flavor: Roasted malt aromas and flavors including cocoa are acceptable at low levels. A very low level of malt sweetness may be present and balanced by acidity from <i>Lactobacillus</i>.. Hop aroma -  Flavor: Not present.  Very low to medium-low, though acidity and wood-aging (if used) may mask higher bitterness levels. bitterness. Fermentation - <i>Brettanomyces</i>-produced aromas and flavors should be absent or very low. Fruity esters expressed as cherry or green apple attributes are apparent. Overall flavor of Oud Bruin is fundamentally characterized by low to high lactic sourness. Many versions express very low to medium acetic sourness and aroma; acetic sourness may also be absent.. Low to medium - low, with a refreshing mouthfeel body.",
        "moderated": true,
        "minIBU": 5,
        "maxIBU": 18,
        "minABV": 4.8,
        "maxABV": 6.6
    },
    {
        "name": "Belgian-Style Lambic",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Gold to medium amber. Cloudiness is acceptable. Malt aroma - Flavor: Sweet malt character should not be present. Hop aroma -  Flavor: Not present to very low, and can include cheesy, ﬂoral or other attributes. Hop character is achieved by using stale and aged hops at low rates..  Very low bitterness. Fermentation - Characteristic horsey, goaty, leathery and phenolic aromas and ﬂavors derived from <i>Brettanomyces</i> yeast are often present at moderate levels. High to very high fruity esters are present. Traditionally, Lambics are unblended and spontaneously fermented. They express high to very high levels of fruity esters as well as bacteria and yeast-derived sourness. Some versions are fermented with the addition of cultured yeast and bacteria. Carbonation can range from absent to medium. Vanillin and other wood-derived ﬂavors may range from absent to present at up to low-medium levels.. Very low with dry mouthfeel body.",
        "moderated": true,
        "minIBU": 11,
        "maxIBU": 23,
        "minABV": 5,
        "maxABV": 7.6
    },
    {
        "name": "Traditional Belgian-Style Gueuze",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Gold to medium amber. Appearance can range from clear to low-medium cloudiness arising from yeast, as Gueuze is traditionally bottle conditioned.. Malt aroma - Flavor: Sweet malt character is not present. Hop aroma -  Flavor: Not present to very low and can include cheesy, ﬂoral or other attributes..  Very low bitterness. Fermentation - Gueuze represents blends of aged and newly fermenting young Lambics. These unﬂavored blended and secondary fermented beers may range from very dry or mildly sweet. They are characterized by intense fruity ester, sour, and acidic attributes which only result from spontaneous fermentation. Diacetyl should not be present. Characteristic horsey, goaty, leathery and phenolic aromas and ﬂavors derived from <i>Brettanomyces</i> yeast are often present at moderate levels. Vanillin and other wood-derived ﬂavors may range from absent to present at up to low-medium levels. Carbonation can range from absent to high.. Typically very low with dry mouthfeel. Highly complex flavor profile can lend an impression of fullness. body.",
        "moderated": true,
        "minIBU": 11,
        "maxIBU": 23,
        "minABV": 5,
        "maxABV": 8
    },
    {
        "name": "Contemporary Belgian-Style Spontaneous Fermented Ale",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Gold to very dark. Cloudiness is acceptable, as these beers are frequently bottle conditioned.. Malt aroma - Flavor: Sweet malt character is not present. Some versions may exhibit attributes typical of specialty malts.. Hop aroma -  Flavor: Not present to low and can include cheesy, ﬂoral or other attributes typical of aged or unaged hops..  Very low bitterness. Fermentation - These blended and secondary fermented beers may be very dry or mildly sweet. They are characterized by intense fruity ester, sour, and acidic attributes which only result from spontaneous fermentation. Diacetyl should not be present. Characteristic horsey, goaty, leathery, and phenolic aromas and ﬂavors derived from <i>Brettanomyces</i> yeast are often present at moderate levels. Aged beer is often blended with young beer to create this special style. Vanillin and other wood-derived ﬂavors may range from absent to present at up to low-medium levels. Carbonation can be none (flat) to high.. Very low with dry mouthfeel body.",
        "moderated": true,
        "minIBU": 11,
        "maxIBU": 23,
        "minABV": 5,
        "maxABV": 8.9
    },
    {
        "name": "Belgian-Style Fruit Lambic",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - Often influenced by the color of added fruit. Cloudiness is acceptable. Malt aroma - Flavor: Malt sweetness should be absent, but sweetness of fruit may be low to high.. Hop aroma -  Flavor: Hop aroma and ﬂavor is not present. Cheesy hop character should not be present..  Very low bitterness. Fermentation - Characteristic horsey, goaty, leathery, and phenolic aromas and ﬂavors derived from <i>Brettanomyces</i> yeast are often present at moderate levels. Fermented sourness is an important part of the ﬂavor proﬁle, though sweetness may compromise the intensity. Fruit sourness may also be an important part of the proﬁle. These ﬂavored Lambic beers may be very dry or mildly sweet.. Dry to full body.",
        "moderated": true,
        "minIBU": 15,
        "maxIBU": 21,
        "minABV": 5,
        "maxABV": 8.9
    },
    {
        "name": "Other Belgian-Style Ale",
        "typing": [
            "Ale Styles",
            "Belgian and French Origin Ale Styles"
        ],
        "description": "Color - May vary widely. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Malt perception may vary widely. Hop aroma -  Flavor: May vary widely.  May vary widely bitterness. Fermentation - Phenolic spiciness may be absent or may be present at low levels. Fruity-ester complexity may range from low to medium, in harmony with malt and other attributes. Diacetyl should not be present.. Varies with style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Grodziskie",
        "typing": [
            "Ale Styles",
            "Other Origin Ale Styles"
        ],
        "description": "Color - Straw to gold. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Oak-smoked wheat malt comprises the entire grain bill. Assertive smoked wheat malt aromas and flavors are medium to medium-high with aroma dominated by oak smoke.. Hop aroma -  Flavor: Aroma and flavor of noble hops ranges from not present to low.  Medium-low to medium bitterness. Fermentation - Fruity esters are low. Diacetyl and DMS should not be present. An overall crisp flavor is achieved by managing fermentation temperatures. Sourness should not be present.. Low to medium - low body.",
        "moderated": true,
        "minIBU": 15,
        "maxIBU": 25,
        "minABV": 2.7,
        "maxABV": 3.7
    },
    {
        "name": "Adambier",
        "typing": [
            "Ale Styles",
            "Other Origin Ale Styles"
        ],
        "description": "Color - Light brown to very dark. Beer color may be too dark to perceive clarity. When clarity is perceivable, chill haze is absent.. Malt aroma - Flavor: Toast and caramel malt aroma and flavor may be present. Astringency from highly roasted malt should not be present.. Hop aroma -  Flavor: Low, with attributes typical of traditional non-hybrid European hop varieties..  Low to medium bitterness. Fermentation - A cool ale fermentation is typically used. Extensive aging and acidiﬁcation can mask malt and hop character to varying degrees. Aging in barrels may contribute some level of <i>Brettanomyces</i> and lactic character.. Medium, to full body.",
        "moderated": true,
        "minIBU": 30,
        "maxIBU": 50,
        "minABV": 9,
        "maxABV": 11
    },
    {
        "name": "Dutch-Style Kuit, Kuyt or Koyt",
        "typing": [
            "Ale Styles",
            "Other Origin Ale Styles"
        ],
        "description": "Color - Gold to copper. Chill haze and other haze is acceptable. Malt aroma - Flavor: The aroma is grainy or grainy-bready. A distinctive aroma and flavor profile arises from the use of at least 45 percent oat malt, and at least 20 percent wheat malt. Pale malt makes up the remainder of the grain bill.. Hop aroma -  Flavor: Very low to low from noble hops or other traditional European varieties.  Medium-low to medium bitterness. Fermentation - Esters may be present at low levels. Diacetyl is usually absent in these beers but may be present at very low levels. Acidity and sweet corn-like DMS should not be present.. Low to medium body.",
        "moderated": true,
        "minIBU": 25,
        "maxIBU": 35,
        "minABV": 4.7,
        "maxABV": 7.9
    },
    {
        "name": "International-Style Pale Ale",
        "typing": [
            "Ale Styles",
            "Other Origin Ale Styles"
        ],
        "description": "Color - Gold to copper. Chill haze is acceptable at low temperatures. Malt aroma - Flavor: Very low to medium malt flavor and aroma should be present. Low caramel malt aroma and flavor may be present.. Hop aroma -  Flavor: Hop aroma is low to high. Hop ﬂavor is very low to high. Hop character can vary widely, exhibiting diverse hop aroma and flavor attributes..  Medium to high bitterness. Fermentation - Fruity esters are low to high. Diacetyl is usually absent in these beers but may be present at very low levels. DMS should not be present.. Low to medium body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 42,
        "minABV": 4.4,
        "maxABV": 6.6
    },
    {
        "name": "Classic Australian-Style Pale Ale",
        "typing": [
            "Ale Styles",
            "Other Origin Ale Styles"
        ],
        "description": "Color - Straw to copper. Chill or yeast haze is acceptable at low levels. Malt aroma - Flavor: Low malt sweetness and other malt attributes are present. Hop aroma -  Flavor: Low to medium.  Low to medium bitterness. Fermentation - Perceivable fruity esters should be present, and are a defining character of this beer style, balanced by low to medium hop aroma. Overall flavor impression is mild. Clean yeasty, bready character may be present. Yeast in suspension if present may impact overall perception of bitterness. Diacetyl is usually absent in these beers but may be present at very low levels. DMS should not be present.. Low to medium, with a dry finish body.",
        "moderated": true,
        "minIBU": 15,
        "maxIBU": 35,
        "minABV": 4,
        "maxABV": 6
    },
    {
        "name": "Australian-Style Pale Ale",
        "typing": [
            "Ale Styles",
            "Other Origin Ale Styles"
        ],
        "description": "Color - Straw to medium amber. Any of yeast, chill, or hop haze may be present in this style at low levels but are not essential. Malt aroma - Flavor: Very low to medium. Hop aroma -  Flavor: Medium-low to medium-high, exhibiting attributes typical of modern hop varieties including any of tropical fruit, mango, passionfruit, or stone fruit.  Low to medium-high bitterness. Fermentation - Very low to low fruity esters are acceptable but not essential.. Low to medium - low, with a dry finish body.",
        "moderated": true,
        "minIBU": 15,
        "maxIBU": 40,
        "minABV": 4,
        "maxABV": 6
    },
    {
        "name": "New Zealand-Style Pale Ale",
        "typing": [
            "Ale Styles",
            "Other Origin Ale Styles"
        ],
        "description": "Color - Straw to medium amber. Any of yeast, chill, or hop haze may be present in at low levels but are not essential. Malt aroma - Flavor: Very low to medium. Hop aroma -  Flavor: Medium to medium-high, exhibiting attributes including any of tropical fruit, passionfruit, stone fruit, cut grass, or diesel.  Low to medium-high bitterness. Fermentation - Low to medium fruity esters are acceptable but not essential.. Medium-low to medium with a dry finish body.",
        "moderated": true,
        "minIBU": 15,
        "maxIBU": 40,
        "minABV": 4,
        "maxABV": 6
    },
    {
        "name": "New Zealand-Style India Pale Ale",
        "typing": [
            "Ale Styles",
            "Other Origin Ale Styles"
        ],
        "description": "Color - Gold to copper. Chill haze is acceptable at low temperatures. Hop haze is allowable at any temperature.. Malt aroma - Flavor: Low to medium intensity malt attributes are present in aroma and ﬂavor. Hop aroma -  Flavor: High to intense, exhibiting attributes such as ﬂoral, fruity (tropical, stone fruit, and other), sulfur/diesel-like, citrusy, and grassy.  Medium-high to very high bitterness. Fermentation - Fruity esters are low to high, acceptable but not essential.. Medium-low to medium with a dry finish body.",
        "moderated": true,
        "minIBU": 50,
        "maxIBU": 70,
        "minABV": 6.3,
        "maxABV": 7.5
    },
    {
        "name": "Finnish-Style Sahti",
        "typing": [
            "Ale Styles",
            "Other Origin Ale Styles"
        ],
        "description": "Color - Pale to copper. Chill haze, yeast haze and general turbidity is acceptable.. Malt aroma - Flavor: Malt aroma is medium-low to medium. Malt ﬂavor is medium to high with malt sweetness present.. Hop aroma -  Flavor: Not present to very low.  Very low bitterness. Fermentation - These beers can vary significantly in character. Fruity ester and yeasty aromas are medium to high. Diacetyl should not be present. Bread/bakers’ yeast is traditionally used for fermentation and may produce flavors and aromas of complex alcohols, clove-like phenolics and banana fruitiness.. Medium, to full body.",
        "moderated": true,
        "minIBU": 3,
        "maxIBU": 16,
        "minABV": 7,
        "maxABV": 8.5
    },
    {
        "name": "Swedish-Style Gotlandsdricke",
        "typing": [
            "Ale Styles",
            "Other Origin Ale Styles"
        ],
        "description": "Color - Pale to copper. Chill haze or yeast haze is acceptable. Malt aroma - Flavor: Medium-low to medium. Birchwood smoke character, derived from the malting process, should be present.. Hop aroma -  Flavor: Not present to very low.  Very low to medium-low bitterness. Fermentation - Bread/bakers’ yeast is traditionally used for fermentation and contributes to unique character of these beers. Fruity ester and yeasty aromas are medium to high. Diacetyl should not be present.. Medium, to full body.",
        "moderated": true,
        "minIBU": 15,
        "maxIBU": 25,
        "minABV": 5.5,
        "maxABV": 6.5
    },
    {
        "name": "Breslau-Style Schoeps",
        "typing": [
            "Ale Styles",
            "Other Origin Ale Styles"
        ],
        "description": "Color - Straw to black. Chill haze is acceptable at low temperatures. Hue may be too dark to perceive clarity in some versions.. Malt aroma - Flavor: Malt sweetness is medium to medium-high with a pronounced malt character. A high proportion of pale or dark wheat malt (as much as 80 percent) is used to brew these beers as well as Pilsener and other pale, toasted, or dark specialty malts. Paler versions may have bready, aromatic biscuit malt attributes. Darker versions may exhibit roast malt bitterness at low levels, and toasted or nutty malt attributes. Caramel-like malt attributes are not present.. Hop aroma -  Flavor: Very low.  Medium-low to medium bitterness. Fermentation - Fruity esters may be present as these beers are fermented with ale yeast as opposed to wheat beer yeast. Diacetyl and phenolic aromas and ﬂavors should not be present.. Full body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 30,
        "minABV": 6,
        "maxABV": 7
    },
    {
        "name": "German-Style Leichtbier",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Straw to pale. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Low to medium. Hop aroma -  Flavor: Low to medium.  Medium bitterness. Fermentation - Fruity esters and diacetyl should not be present. Very low levels of sulfur-related compounds are acceptable.. Very low body.",
        "moderated": true,
        "minIBU": 16,
        "maxIBU": 24,
        "minABV": 2.5,
        "maxABV": 3.7
    },
    {
        "name": "German-Style Pilsener",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Straw to gold. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: A malty sweet aroma and flavor should be present at low levels. Bready or light biscuity attributes may be present.. Hop aroma -  Flavor: Hop aroma and ﬂavor is moderate and pronounced, derived from late hopping (not dry hopping) with noble-type hops. Floral, herbal, peppery, or other attributes may be present..  Medium to high bitterness. Fermentation - Fruity-ester and DMS should not be present. These are well attenuated beers.. Low to medium - low body.",
        "moderated": true,
        "minIBU": 25,
        "maxIBU": 50,
        "minABV": 4.6,
        "maxABV": 5.3
    },
    {
        "name": "Bohemian-Style Pilsener",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Straw to gold. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: A slightly sweet and toasted, biscuity, bready malt aroma and ﬂavor is present.. Hop aroma -  Flavor: Medium-low to medium, derived from late kettle hopping with noble-type hops..  Medium bitterness. Fermentation - The upper limit of Original Gravity of versions brewed in Czech Republic is 12.99 °Plato or 1.052 specific gravity. Esters are usually not present, but if present should be extremely low, at the limit of perception. Very low levels of diacetyl, if present, are acceptable and may accent malt character. Low levels of sulfur compounds may be present. DMS and acetaldehyde should not be present.. Medium body.",
        "moderated": true,
        "minIBU": 25,
        "maxIBU": 45,
        "minABV": 4.1,
        "maxABV": 5.1
    },
    {
        "name": "Munich-Style Helles",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Pale to gold. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Malt aroma and ﬂavor are pronounced. Low levels of yeast-produced sulfur aromas and ﬂavors may be present. Malt character is sometimes bready and suggestive of lightly toasted malted barley. There should be no caramel character.. Hop aroma -  Flavor: Hop aroma is not present to low. Hop ﬂavor is very low to low, derived from noble-type hops..  Low, derived from European noble-type hops. bitterness. Fermentation - Fruity esters, DMS, and diacetyl should not be present. A very low level of sulfur attributes may be present in balance with other attributes.. Medium body.",
        "moderated": true,
        "minIBU": 18,
        "maxIBU": 25,
        "minABV": 4.8,
        "maxABV": 5.6
    },
    {
        "name": "Dortmunder/European-Style Export",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Straw to deep gold. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Sweet malt character should be low and should not be caramelly. Hop aroma -  Flavor: Very low to low, derived from noble-type hops..  Medium bitterness. Fermentation - Fruity esters and diacetyl should not be present.. Medium body.",
        "moderated": true,
        "minIBU": 23,
        "maxIBU": 29,
        "minABV": 5.1,
        "maxABV": 6.1
    },
    {
        "name": "Vienna-Style Lager",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Deep Gold to reddish-brown. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Characterized by malty aroma and light malt sweetness, which should have a lightly toasted malt character.. Hop aroma -  Flavor: Very low to low, derived from noble-type hops..  Low to medium-low, clean, and crisp. bitterness. Fermentation - DMS, diacetyl, and fruity esters should not be present.. Medium body.",
        "moderated": true,
        "minIBU": 22,
        "maxIBU": 28,
        "minABV": 4.8,
        "maxABV": 5.4
    },
    {
        "name": "Franconian-Style Rotbier",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Amber to dark red. Clear to slightly hazy for unfiltered versions. Chill haze should not be present.. Malt aroma - Flavor: Light toasted malt aroma and malt sweetness is typical. Light caramel or biscuit character may be present.. Hop aroma -  Flavor: Low to medium-low, with attributes typical of noble-type hops..  Low to medium-low, producing a clean finish. bitterness. Fermentation - DMS, diacetyl, fruity esters, and phenolic attributes should not be present.. Medium body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 28,
        "minABV": 4.8,
        "maxABV": 5.6
    },
    {
        "name": "German-Style Maerzen",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Pale to reddish-brown. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Bready or biscuity malt aroma and flavor should be present. Sweet maltiness is medium-low to medium and leads to a muted clean hop bitterness. Malt ﬂavors should be of light toast rather than strong caramel. Low level caramel character is acceptable.. Hop aroma -  Flavor: Low with attributes typical of noble hop varieties.  Medium-low to medium bitterness. Fermentation - Fruity esters and diacetyl should not be present. Medium body.",
        "moderated": true,
        "minIBU": 18,
        "maxIBU": 25,
        "minABV": 5.1,
        "maxABV": 6
    },
    {
        "name": "German-Style Oktoberfest/Wiesn",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Straw to gold. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Clean, sweet, bready malt profile is low to medium-low. Hop aroma -  Flavor: Very low to low.  Very low to low and in balance with low sweet maltiness bitterness. Fermentation - Fruity esters and diacetyl should not be present. Medium body.",
        "moderated": true,
        "minIBU": 23,
        "maxIBU": 29,
        "minABV": 5.1,
        "maxABV": 6.1
    },
    {
        "name": "Munich-Style Dunkel",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Light brown to brown. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Malt character is low to medium, with chocolate, roast, bread, or biscuit aromas and flavors contributed by using dark Munich malt or other specialty malts.. Hop aroma -  Flavor: Very low to low, with attributes typical of noble-type hops..  Medium-low to medium bitterness. Fermentation - Fruity esters and diacetyl should not be present. Low to medium - low body.",
        "moderated": true,
        "minIBU": 16,
        "maxIBU": 25,
        "minABV": 4.8,
        "maxABV": 5.3
    },
    {
        "name": "European-Style Dark Lager",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Light brown to dark brown. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Low to medium with chocolate, roast, and malt aromas and flavors present.. Hop aroma -  Flavor: Very low to low with attributes typical of noble-type hops..  Medium-low to medium-high bitterness. Fermentation - Fruity esters should not be present. In most examples diacetyl should not be present, however some Czech dark lagers allow for low levels of diacetyl. Low to medium - low body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 35,
        "minABV": 4.8,
        "maxABV": 5.3
    },
    {
        "name": "German-Style Schwarzbier",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Very dark brown to black, with a pale-colored head.. Beer color may be too dark to perceive. When clarity is perceivable, chill haze should not be present.. Malt aroma - Flavor: Medium malt aroma displays a mild roasted malt character. Malt sweetness is low to medium and displays a mild roasted malt character without bitterness.. Hop aroma -  Flavor: Hop aroma and ﬂavor is very low to low, derived from noble-type hops..  Low to medium bitterness. Fermentation - Fruity esters and diacetyl should not be present.. Low to medium - low body.",
        "moderated": true,
        "minIBU": 22,
        "maxIBU": 30,
        "minABV": 3.8,
        "maxABV": 4.9
    },
    {
        "name": "Bamberg-Style Helles Rauchbier",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Light pale to gold. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Malt character is prominent with malt aromas suggesting lightly toasted sweet, malted barley. Smoke beechwood character ranges from very low to medium. Smoky aroma should be not harshly phenolic. Sulfur may be present at low levels. There should be no caramel character. Smoke ﬂavor may create a perception of mild sweetness.. Hop aroma -  Flavor: Hop aroma and ﬂavor is very low to low, derived from noble-type hops..  Low to medium bitterness. Fermentation - Fruity esters and diacetyl should not be present. Very low levels of sulfur-related compounds are acceptable.. Medium body.",
        "moderated": true,
        "minIBU": 18,
        "maxIBU": 25,
        "minABV": 4.8,
        "maxABV": 5.6
    },
    {
        "name": "Bamberg-Style Maerzen Rauchbier",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Pale to light brown. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Sweet toasted malt aroma should be present. Medium-low to medium toasted malt sweetness should be present. Aroma and flavor of smoked beechwood ranges from very low to medium. Smoke ﬂavors should be smooth, without harshness. Aroma should strike a balance between malt, hop, and smoke.. Hop aroma -  Flavor: Hop aroma and ﬂavor is very low to low, derived from noble-type hops..  Low to medium bitterness. Fermentation - Fruity esters and diacetyl should not be present. Full body.",
        "moderated": true,
        "minIBU": 18,
        "maxIBU": 25,
        "minABV": 5.1,
        "maxABV": 6
    },
    {
        "name": "Bamberg-Style Bock Rauchbier",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Dark brown to very dark. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Medium to medium-high malt aroma and ﬂavor should be present with very low to medium-high beechwood smoke aromas and ﬂavors. Smoke flavors should be smooth, without harshness. Smoke ﬂavor may create a perception of mild sweetness.. Hop aroma -  Flavor: Very low.  Medium, increasing proportionately with starting gravity. bitterness. Fermentation - Fruity esters are usually absent, but if present should be very low. Diacetyl should not be present.. Medium, to full body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 30,
        "minABV": 6.3,
        "maxABV": 7.6
    },
    {
        "name": "German-Style Heller Bock/Maibock",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Pale to light amber. The German word ‘helle’ means light-colored, thus Heller Bock is a pale beer.. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Light toasty or bready aroma and flavor attributes are often present. Roast or heavy toast/caramel malt aromas and ﬂavors should not be present.. Hop aroma -  Flavor: Low to medium-low, derived from noble-type hops..  Low to medium-low bitterness. Fermentation - Fruity esters, if present, should be low. Diacetyl should not be present.. Medium, to full body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 38,
        "minABV": 6.3,
        "maxABV": 8.1
    },
    {
        "name": "Traditional German-Style Bock",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Dark brown to very dark. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Traditional Bocks are made with all malt and have high malt character with aromas of toasted or nutty malt, but not caramel. Traditional bocks display high malt sweetness. The malt ﬂavor profile should display a balance of sweetness and toasted or nutty malt, but not caramel.. Hop aroma -  Flavor: Very low.  Medium, increasing proportionately with starting gravity. bitterness. Fermentation - Fruity esters if present should be minimal. Diacetyl should not be present.. Medium, to full body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 30,
        "minABV": 6.3,
        "maxABV": 7.6
    },
    {
        "name": "German-Style Doppelbock",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Copper to dark brown. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Pronounced aromas and flavors of toasted malted barley. Some caramel and toffee character can contribute to complexity in a secondary role. Dark fruit flavors such as prune and raisin may be present. Malty sweetness is pronounced but should not be cloying. There should be no astringency from roasted malts.. Hop aroma -  Flavor: Hop aroma is absent. Hop ﬂavor is low..  Low bitterness. Fermentation - Alcoholic strength is high. Fruity esters are commonly perceived at low to moderate levels. Diacetyl should not be present.. Full body.",
        "moderated": true,
        "minIBU": 17,
        "maxIBU": 27,
        "minABV": 6.6,
        "maxABV": 7.9
    },
    {
        "name": "German-Style Eisbock",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Light brown to black. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Sweet malt character is very high. Dark fruit flavors such as prune and raisin may be present. Hop aroma -  Flavor: Hop aroma and ﬂavor is absent.  Very low to low bitterness. Fermentation - Alcohol may be present in aroma. Fruity esters may be evident, but not overpowering. Diacetyl should not be present. Alcoholic strength is very high.. Very full body.",
        "moderated": true,
        "minIBU": 26,
        "maxIBU": 33,
        "minABV": 8.6,
        "maxABV": 14.3
    },
    {
        "name": "Chocolate or Cocoa Beer",
        "typing": [
            "Lager Styles",
            "European Origin Lager Styles"
        ],
        "description": "Color - Can range from pale to very dark depending on the underlying style. Clear to hazy is acceptable. Malt aroma - Flavor: Medium-low to medium-high malt sweetness balanced with cocoa ﬂavors and aromas. Hop aroma -  Flavor: Hop aroma may vary based on underlying style and often may be lower than is designated for underlying style allowing chocolate to contribute to the ﬂavor proﬁle without becoming excessively bitter..  Very low to medium-low bitterness. Fermentation - Typical of underlying beer style. Attributes derived from chocolate or cocoa should be apparent in all such beers, ranging from subtle to intense, and in harmony with the overall flavor profile of the beer.. Varies with underlying style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "American-Style Lager",
        "typing": [
            "Lager Styles",
            "North American Origin Lager Styles"
        ],
        "description": "Color - Straw to gold. Chill haze should not be present. Malt aroma - Flavor: Malt sweetness is very low to low. Hop aroma -  Flavor: Not present to very low.  Not present to very low bitterness. Fermentation - Fruity esters are usually absent, but may be present at very low levels. Diacetyl, acetaldehyde, and DMS should not be present.. Low body.",
        "moderated": true,
        "minIBU": 5,
        "maxIBU": 15,
        "minABV": 4.1,
        "maxABV": 5.1
    },
    {
        "name": "Contemporary American-Style Lager",
        "typing": [
            "Lager Styles",
            "North American Origin Lager Styles"
        ],
        "description": "Color - Straw to gold. Chill haze should not be present. Malt aroma - Flavor: Malt sweetness and aroma are very low to low. Hop aroma -  Flavor: Very low to low.  Very low to low bitterness. Fermentation - Fruity esters are usually absent but may be present at very low levels. Diacetyl, acetaldehyde, and DMS should not be present.. Low body.",
        "moderated": true,
        "minIBU": 5,
        "maxIBU": 16,
        "minABV": 4.1,
        "maxABV": 5.1
    },
    {
        "name": "American-Style Light Lager",
        "typing": [
            "Lager Styles",
            "North American Origin Lager Styles"
        ],
        "description": "Color - Very light to pale. Chill haze should not be present. Malt aroma - Flavor: Very low. Hop aroma -  Flavor: Absent to very low.  Absent to very low bitterness. Fermentation - Fruity esters are usually absent but may be present at very low levels. Diacetyl, acetaldehyde, and DMS should not be present. These beers are characterized by an extremely high degree of attenuation. Final gravity is often less than 1.000 (0.0 ºPlato).. Low with dry mouthfeel body.",
        "moderated": true,
        "minIBU": 4,
        "maxIBU": 10,
        "minABV": 3.5,
        "maxABV": 4.4
    },
    {
        "name": "Contemporary American-Style Light Lager",
        "typing": [
            "Lager Styles",
            "North American Origin Lager Styles"
        ],
        "description": "Color - Very light to medium amber. The word ‘Light’ refers to light body and reduced calories rather than color.. Chill haze should not be present. Malt aroma - Flavor: Very low but present. Hop aroma -  Flavor: Very low to low.  Very low to low bitterness. Fermentation - Fruity esters are usually absent but may be present at very low levels. Diacetyl, acetaldehyde, and DMS should not be present. These beers are characterized by an extremely high degree of attenuation. Final gravity is often less than 1.000 (0.0 ºPlato).. Low to medium - low, often with dry mouthfeel body.",
        "moderated": true,
        "minIBU": 4,
        "maxIBU": 15,
        "minABV": 3.5,
        "maxABV": 4.4
    },
    {
        "name": "American-Style Pilsener",
        "typing": [
            "Lager Styles",
            "North American Origin Lager Styles"
        ],
        "description": "Color - Straw to gold. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Medium-low to medium. Hop aroma -  Flavor: Medium to high, exhibiting attributes typical of noble-type hops.  Medium to medium-high bitterness. Fermentation - DMS, acetaldehyde, fruity esters, and diacetyl should not be present.. Medium-low to medium body.",
        "moderated": true,
        "minIBU": 25,
        "maxIBU": 40,
        "minABV": 4.9,
        "maxABV": 6
    },
    {
        "name": "Contemporary American-Style Pilsener",
        "typing": [
            "Lager Styles",
            "North American Origin Lager Styles"
        ],
        "description": "Color - Straw to gold. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Medium-low to medium. Hop aroma -  Flavor: Medium to high. While traditional versions exhibit attributes typical of noble-type hops, contemporary versions will exhibit attributes typical of a wide range of hop varieties..  Medium to medium-high bitterness. Fermentation - DMS, acetaldehyde, fruity esters, and diacetyl should not be present.. Medium-low to medium body.",
        "moderated": true,
        "minIBU": 25,
        "maxIBU": 50,
        "minABV": 4.9,
        "maxABV": 6
    },
    {
        "name": "American-Style India Pale Lager",
        "typing": [
            "Lager Styles",
            "North American Origin Lager Styles"
        ],
        "description": "Color - Straw to gold. Hop haze is allowable. Chill haze should not be present. Malt aroma - Flavor: Very low to medium, and may exhibit bready, cracker-like, or other attributes typical of pale malts. Hop aroma -  Flavor: Medium to high with attributes typical of hops of any origin.  Medium to high, but not harsh bitterness. Fermentation - Fruity esters range from absent to medium-low. DMS, acetaldehyde, and diacetyl should not be present.. Low to medium body.",
        "moderated": true,
        "minIBU": 30,
        "maxIBU": 70,
        "minABV": 5.6,
        "maxABV": 7.9
    },
    {
        "name": "American-Style Malt Liquor",
        "typing": [
            "Lager Styles",
            "North American Origin Lager Styles"
        ],
        "description": "Color - Straw to gold. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Some malt sweetness is present. Hop aroma -  Flavor: Not present.  Very low bitterness. Fermentation - Fruity esters and complex alcohol aromas and ﬂavors are acceptable at low levels. Alcohol should not be solvent-like. DMS, diacetyl, and acetaldehyde should not be present.. Low to medium - low body.",
        "moderated": true,
        "minIBU": 12,
        "maxIBU": 23,
        "minABV": 6.3,
        "maxABV": 7.6
    },
    {
        "name": "American-Style Amber Lager",
        "typing": [
            "Lager Styles",
            "North American Origin Lager Styles"
        ],
        "description": "Color - Gold to copper. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Low to medium-low caramel or toasted malt aromas and ﬂavors should be present. Hop aroma -  Flavor: Very low to medium-high.  Very low to medium-high bitterness. Fermentation - Fruity esters and diacetyl should not be present. Medium body.",
        "moderated": true,
        "minIBU": 18,
        "maxIBU": 30,
        "minABV": 4.8,
        "maxABV": 5.4
    },
    {
        "name": "American-Style Maerzen/Oktoberfest",
        "typing": [
            "Lager Styles",
            "North American Origin Lager Styles"
        ],
        "description": "Color - Pale to reddish brown. Chill haze should not be present. Malt aroma - Flavor: Sweet maltiness should be present, expressed as a light toasted character. Bready or biscuity malt aroma and ﬂavor is acceptable. Low level caramel attributes are acceptable.. Hop aroma -  Flavor: Low to medium-low exhibiting herbal, grass-like, spicy, floral, or citrus attributes.  Medium-low to medium bitterness. Fermentation - Fruity esters and diacetyl should not be present. Medium body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 30,
        "minABV": 5.1,
        "maxABV": 6
    },
    {
        "name": "American-Style Dark Lager",
        "typing": [
            "Lager Styles",
            "North American Origin Lager Styles"
        ],
        "description": "Color - Light brown to very dark. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Low malt aroma and ﬂavor may include low levels of caramel. Hop aroma -  Flavor: Very low to low.  Very low to low and dissipates quickly. bitterness. Fermentation - Carbonation is high. Fruity esters, DMS and diacetyl should not be present.. Low body.",
        "moderated": true,
        "minIBU": 14,
        "maxIBU": 24,
        "minABV": 4.1,
        "maxABV": 5.6
    },
    {
        "name": "Australasian, Latin American or Tropical-Style Light Lager",
        "typing": [
            "Lager Styles",
            "Other Origin Lager Styles"
        ],
        "description": "Color - Straw to gold. Chill haze should not be present. Malt aroma - Flavor: Malt sweetness is absent. Hop aroma -  Flavor: Not present to very low.  Very low bitterness. Fermentation - Sugar adjuncts are often used to lighten the body and ﬂavor, sometimes contributing to very low to low fruity esters such as apple or pear. DMS, diacetyl, and acetaldehyde should not be present.. Low body.",
        "moderated": true,
        "minIBU": 9,
        "maxIBU": 18,
        "minABV": 4.1,
        "maxABV": 5.1
    },
    {
        "name": "International-Style Pilsener",
        "typing": [
            "Lager Styles",
            "Other Origin Lager Styles"
        ],
        "description": "Color - Straw to gold. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Residual malt aroma and ﬂavor may be present at low to medium-low levels. Hop aroma -  Flavor: Low to medium.  Low to medium bitterness. Fermentation - Very low levels of DMS aroma and ﬂavor are acceptable. Fruity esters, acetaldehyde, and diacetyl should not be present.. Low to medium body.",
        "moderated": true,
        "minIBU": 17,
        "maxIBU": 40,
        "minABV": 4.6,
        "maxABV": 5.3
    },
    {
        "name": "Baltic-Style Porter",
        "typing": [
            "Lager Styles",
            "Other Origin Lager Styles"
        ],
        "description": "Color - Black. Opaque. When clarity is perceivable, chill haze should not be present.. Malt aroma - Flavor: Malt sweetness is medium-low to medium-high. Distinctive malt aromas and flavors of caramelized sugars, dark sugars, and licorice are present. Roast malt attributes may be present at low levels, but any bitterness or astringency should be in harmony with other flavor aspects.. Hop aroma -  Flavor: Very low. Floral hop aroma can complement aromatics..  Low to medium-low bitterness. Fermentation - Due to its alcoholic strength, there may be very low to low levels of complex alcohol aromas and ﬂavors, and higher levels of fruitiness suggestive of berries, grapes, or plums, but not banana. Fruity esters, DMS, and diacetyl should not be present.. Medium, to full body.",
        "moderated": true,
        "minIBU": 35,
        "maxIBU": 40,
        "minABV": 7.6,
        "maxABV": 9.3
    },
    {
        "name": "Session Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - The color should mimic the classic style upon which the beer is based. Appearance may vary from brilliant to hazy to cloudy and should mimic the classic style upon which the beer is based. Malt aroma - Flavor: Malt attributes should mimic the classic style upon which the beer is based, but at lower overall intensity due to lower Original Gravity.. Hop aroma -  Flavor: Hop attributes should mimic the classic style upon which the beer is based, but at lower overall intensity in order to maintain the balance typical of that style..  Should mimic the classic style upon which the beer is based, but at lower overall intensity in order to maintain the balance typical of that style. bitterness. Fermentation - Varies with underlying style. Varies with underlying style body.",
        "moderated": true,
        "minIBU": 10,
        "maxIBU": 35,
        "minABV": 0.5,
        "maxABV": 5
    },
    {
        "name": "American-Style Cream Ale",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Straw to gold. Chill haze should be very low or not be present. Malt aroma - Flavor: The dominant flavor is of pale malt sweetness at medium-low to medium levels. Caramel malt attributes should be absent. Attributes typical of corn or other adjuncts may be present at low levels.. Hop aroma -  Flavor: Hop aroma and ﬂavor is very low to low or may be absent.  Very low to low bitterness. Fermentation - Low level fruity esters may be present. Sulfur and DMS are usually absent but may be present at extremely low levels. Diacetyl should not be present.. Low body.",
        "moderated": true,
        "minIBU": 10,
        "maxIBU": 22,
        "minABV": 4.3,
        "maxABV": 5.7
    },
    {
        "name": "California Common Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Light amber to medium amber. Appearance should be clear. Chill haze should not be present. Malt aroma - Flavor: Medium level toasted or caramel malt attributes are present.. Hop aroma -  Flavor: Low to medium-low.  Medium to medium-high bitterness. Fermentation - Fruity esters are low to medium-low. Diacetyl should be absent.. Medium body.",
        "moderated": true,
        "minIBU": 35,
        "maxIBU": 45,
        "minABV": 4.6,
        "maxABV": 5.7
    },
    {
        "name": "Kentucky Common Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Medium to deep amber. Chill haze or yeast haze is acceptable. Malt aroma - Flavor: Medium-low to medium. Sweet malt is the dominant flavor attribute. Any of corn, caramel, toffee, or bready attributes may be present.. Hop aroma -  Flavor: Low to medium. May exhibit floral or spicy attributes typical of early 20th century North American hop varieties..  Low to medium bitterness. Fermentation - Low to medium-low fruity esters may be present. Very low levels of DMS, if present, are acceptable. Diacetyl should not be present.. Medium-low to medium with a dry finish enhanced by high carbonation body.",
        "moderated": true,
        "minIBU": 15,
        "maxIBU": 30,
        "minABV": 4,
        "maxABV": 5.5
    },
    {
        "name": "American-Style Wheat Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Straw to dark brown. Clear to cloudy. Malt aroma - Flavor: Low to medium-low level pale malt attributes are present in paler versions. Medium-low to medium-high malt attributes such as cocoa, chocolate, caramel, toffee, or biscuit may be present in darker versions. Roast malt astringency is acceptable in darker versions when balanced with malt sweetness. . Hop aroma -  Flavor: Low to high.  Low to medium bitterness. Fermentation - low to medium fruity esters are present. Diacetyl and phenolic, clove-like attributes should not be present. Very low to medium body.",
        "moderated": true,
        "minIBU": 10,
        "maxIBU": 35,
        "minABV": 3.5,
        "maxABV": 5.6
    },
    {
        "name": "American-Style Fruit Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Can range from pale to very dark depending on the underlying style and is often influenced by the color of added fruit. Clear or hazy is acceptable. Malt aroma - Flavor: Not present to medium-low. Hop aroma -  Flavor: Not present to medium-low.  In balance with fruit character and usually at very low to medium levels bitterness. Fermentation - American-Style Fruit Beers are fermented with traditional German, British or American ale or lager yeast. Beers fermented with Belgian-style, German-style Hefeweizen or other South German wheat beer or Berliner-style Weisse yeasts should be categorized elsewhere. Fruit beers exhibiting sourness should be categorized elsewhere. Attributes typical of wild fermentation should not be present.. Varies with style body.",
        "moderated": true,
        "minIBU": 5,
        "maxIBU": 70,
        "minABV": 2.5,
        "maxABV": 12
    },
    {
        "name": "Fruit Wheat Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Generally straw to light amber and often influenced by the color of added fruit.. Chill haze is acceptable. These beers may be served with or without yeast. When served with yeast, appearance is hazy to very cloudy.. Malt aroma - Flavor: Low to medium-low. Hop aroma -  Flavor: Low to medium.  Low to medium bitterness. Fermentation - These beers can be fermented with either ale or lager yeast depending on the underlying wheat beer style. Low fruity esters are typical. Diacetyl should not be present. In versions served with yeast, yeasty aroma and ﬂavor should be low to medium.. Low to medium body.",
        "moderated": true,
        "minIBU": 10,
        "maxIBU": 35,
        "minABV": 2.5,
        "maxABV": 12
    },
    {
        "name": "Belgian-Style Fruit Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Can range from pale to dark depending on underlying Belgian style, and is often influenced by the color of added fruit. Clear to hazy beer is acceptable. Malt aroma - Flavor: Can vary from not perceived to medium-high. Hop aroma -  Flavor: Low to high.  Varies with underlying Belgian style bitterness. Fermentation - Acidic bacterial fermentation attributes may be absent or may be present; if present, such attributes contribute to acidity and enhance fruity balance.. Varies with style body.",
        "moderated": true,
        "minIBU": 5,
        "maxIBU": 70,
        "minABV": 2.5,
        "maxABV": 12
    },
    {
        "name": "Field Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Can range from pale to very dark depending on the underlying style and is often influenced by the color of added field ingredients (vegetables, nuts, etc.). Clear to hazy is acceptable. Malt aroma - Flavor: Very low to medium-high. Hop aroma -  Flavor: Very low to medium-high.  Very low to medium-high. Vegetable character should not be muted by hop character. bitterness. Fermentation - Varies with underlying style. Varies with underlying style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Pumpkin Spice Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Can vary from pale to very dark depending on the underlying style. Clear to hazy is acceptable. Malt aroma - Flavor: Can vary from low to medium-high depending on the underlying style. Hop aroma -  Flavor: None to medium and, if present, in harmony with spice, pumpkin or squash, and other attributes..  Low to medium-low bitterness. Fermentation - Typical of underlying beer style. Varies with underlying style body.",
        "moderated": true,
        "minIBU": 5,
        "maxIBU": 35,
        "minABV": 2.5,
        "maxABV": 12
    },
    {
        "name": "Pumpkin/Squash Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Can range from pale to very dark depending on the underlying style. Clear to hazy is acceptable. Malt aroma - Flavor: Can vary from low to medium-high depending on the underlying style. Hop aroma -  Flavor: None to medium.  Low to medium-low bitterness. Fermentation - Typical of underlying beer style. Varies with underlying style body.",
        "moderated": true,
        "minIBU": 5,
        "maxIBU": 35,
        "minABV": 2.5,
        "maxABV": 12
    },
    {
        "name": "Coffee Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Pale to black depending on the underlying style. Clear to hazy is acceptable. Malt aroma - Flavor: Varies with underlying style to provide balance with coffee ﬂavor and aroma. Hop aroma -  Flavor: Low to high depending on the underlying style.  Varies with underlying style bitterness. Fermentation - Typical of underlying style. Reﬂective of the underlying beer style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Chili Pepper Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Can range from pale to very dark depending on the underlying style. Can range from clear to hazy depending on the underlying beer style. Malt aroma - Flavor: Can vary from very low to medium-high depending on the underlying style. Hop aroma -  Flavor: Very low to very high.  Very low to medium-high bitterness. Fermentation - Chili pepper aroma and ﬂavor attributes should be harmonious with the underlying beer style. Chili pepper character may be expressed as vegetal, spicy, or hot on the palate.. Representative of underlying style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Herb and Spice Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Varies depending on underlying style. Clear to hazy is acceptable. Malt aroma - Flavor: Varies depending on intention of brewer, underlying beer style, and intensity of herb or spice aroma attributes. Hop aroma -  Flavor: Not essential but may be present and may be more assertive than herb-spice character.  Very low to medium-low. Reduced hop bitterness tends to accentuate herb/spice character. bitterness. Fermentation - Aromas and ﬂavors of individual spices may not always be identiﬁable. Varies with underlying style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Specialty Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - . . Malt aroma - Flavor. Hop aroma -  Flavor.   bitterness. Fermentation - .  body.",
        "moderated": true,
        "minIBU": 0,
        "maxIBU": 0,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Specialty Honey Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Very light to black depending on underlying style. Clear to hazy is acceptable. Malt aroma - Flavor: Varies depending on intention of brewer. Hop aroma -  Flavor: Very low to very high.  Very low to very high bitterness. Fermentation - Honey Beers may be brewed to a traditional style or may be experimental. Honey Beers incorporate honey as a fermentable sugar in addition to malted barley. Honey character should be present in aroma and ﬂavor but should not be overpowering. Beers which represent various India Pale Ale and Imperial India Pale Ale styles brewed with honey are categorized as Experimental India Pale Ale.. Varies with underlying style body.",
        "moderated": true,
        "minIBU": 1,
        "maxIBU": 100,
        "minABV": 2.5,
        "maxABV": 12
    },
    {
        "name": "Rye Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - A wide range of color is acceptable. Lighter versions are straw to copper, while darker versions are dark amber to dark brown.. Chill haze is acceptable in versions packaged and served without yeast. In versions served with yeast, appearance may range from hazy to very cloudy.. Malt aroma - Flavor: In darker versions, malt aromas and ﬂavors can optionally include low roasted malt character expressed as cocoa/chocolate or caramel. Aromatic toffee, caramel, or biscuit character may also be present. Low level roastiness, graininess, or tannin astringency is acceptable when balanced with low to medium malt sweetness.. Hop aroma -  Flavor: Low to medium-high.  Low to medium bitterness. Fermentation - Low levels of spicy and fruity ester aromas are typical. Yeast-derived aroma and flavor attributes such as clove-like or other phenolics may be present when consistent with underlying beer style. These beers can be fermented with either ale or lager yeast. Diacetyl should not be present. Low to medium yeast aroma may be present in versions packaged with yeast.. Low to medium. Rye can impart textural qualities ranging from dry and crisp to smooth and velvety. body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Brett Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Any color is acceptable. Beer color may be influenced by the color of added fruits or other ingredients.. Chill or yeast haze is allowable at low to medium levels at any temperature. Malt aroma - Flavor: In darker versions, any of roasted malt, caramel or chocolate aromas and ﬂavors are present at low levels.. Hop aroma -  Flavor: Low to high. Low to high bitterness. Fermentation - Medium to high fruity esters are present. Acidity resulting from <i>Brettanomyces</i> fermentation results in a complex flavor profile. <i>Brettanomyces</i> character should be present at low to high levels, expressed as any of horsey, goaty, leathery, phenolic, fruity, or acidic aroma and flavor attributes. <i>Brettanomyces</i> character may or may not be dominant. Acidity from <i>Brettanomyces</i> should be low to medium-low. Cultured yeast strains may be used in the fermentation. Beers fermented with <i>Brettanomyces</i> that do not exhibit attributes typical of <i>Brettanomyces</i> fermentation are categorized elsewhere. Beers in this style should not incorporate bacteria or exhibit a bacteria-derived flavor profile. Diacetyl and DMS should not be present.. Low to high body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Mixed-Culture Brett Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Any color is acceptable. Beer color may be influenced by the color of added fruits or other ingredients.. Chill haze, bacteria and yeast-induced haze is allowable at low to medium levels at any temperature.. Malt aroma - Flavor: In darker versions, any of roasted malt, caramel, or chocolate aromas and ﬂavors are present at low levels.. Hop aroma -  Flavor: Low to high.  Low to high bitterness. Fermentation - Medium to high fruity esters are present. Acidity resulting from fermentation with <i>Brettanomyces</i> or bacteria results in a complex flavor profile. <i>Brettanomyces</i> character should be present and expressed as any of horsey, goaty, leathery, phenolic, fruity, or acidic aroma and flavor attributes. Cultured yeast may be used in the fermentation. Bacteria should be incorporated and in evidence. Bacteria will contribute acidity which may or may not dominate the flavor profile. Diacetyl and DMS should not be present.. Low to high body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Ginjo Beer or Sake-Yeast Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Pale to dark brown. Slight chill haze is acceptable. Malt aroma - Flavor: Very low to medium. Hop aroma -  Flavor: Low to medium and in harmony with sake-like character.  Low to medium and in harmony with sake-like character bitterness. Fermentation - These beers are brewed with sake yeast or sake (koji) enzymes. The unique byproducts of sake yeast and koji enzymes should be distinctive and in harmony with other elements. Sake character may best be described as having mild fruitiness and mild earthiness expressed as mushroom or umami protein-like attributes. A high amount of alcohol may be evident.. Varies depending on Original Gravity. Mouthfeel also varies. body.",
        "moderated": true,
        "minIBU": 12,
        "maxIBU": 35,
        "minABV": 4.3,
        "maxABV": 10.2
    },
    {
        "name": "Fresh Hop Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Straw to gold. Chill haze or hop haze is acceptable at low levels. Malt aroma - Flavor: Low to medium-low. Caramel or roasted malt character should not be present. Hop aroma -  Flavor: High to very high, exhibiting a wide range of attributes including floral, piney, citrus, fruity (berry, tropical, stone fruit and other), sulfur, diesel-like, onion-garlic, catty, resinous and many others..  Medium-high to very high, but not harsh bitterness. Fermentation - Fruity esters range from low to medium. DMS, acetaldehyde, and diacetyl should not be present. These beers are characterized by a high degree of attenuation.. Low to medium body.",
        "moderated": true,
        "minIBU": 50,
        "maxIBU": 75,
        "minABV": 6.3,
        "maxABV": 7.5
    },
    {
        "name": "Wood- and Barrel-Aged Sour Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Varies with underlying style. Varies with underlying style. Malt aroma - Flavor: Varies with underlying style. Hop aroma -  Flavor: Varies with underlying style.  Varies with underlying style bitterness. Fermentation - Typical of underlying style of sour beer being aged. Varies with underlying style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Aged Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Varies with underlying style. Varies with underlying style. Malt aroma - Flavor: Varies with underlying style. Hop aroma -  Flavor: Varies with underlying style.  Varies with underlying style bitterness. Fermentation - Aged Beers are any beers aged for over one year. A brewer may brew any type of beer of any strength and enhance its character with various aging conditions for an extended time. In general, beers with high hopping rates, roast malt, high alcohol content, and complex herbal, smoke or fruit character are the best candidates for aging. Aged Beers may be aged in bottles, cans, kegs or other non-wooden vessels. Aged character may be expressed in mouthfeel, aroma, and ﬂavor. Often, aged character is the result of oxidative reactions that either bring individual flavor components into harmony or are unique flavors unto themselves. Sherry-like and fruity flavors often develop during aging, and hop character often changes. No matter what the effect, the overall character should be balanced and without aggressive flavors. The level of change created by aging will vary with the duration of aging and the underlying beer style. Mildly-ﬂavored beers are more likely to develop aggressive and unpleasant oxidation. Positive transformations are more likely to occur in beers with higher levels of hops, malt, or alcohol.. Varies with underlying style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Experimental Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - May vary widely with ingredients used. Varies with ingredients used and brewing process. Malt aroma - Flavor: May vary widely with ingredients used and brewing process. Hop aroma -  Flavor: May vary widely with ingredients used and brewing process.  May vary widely with ingredients used and brewing process bitterness. Fermentation - Will vary widely depending on the nature of the techniques and or ingredients used to create the beer. May vary widely with ingredients used and brewing process body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Experimental India Pale Ale",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Straw to very dark, varying widely with ingredients used. May range from clear to very high degree of cloudiness. Starch, yeast, hop, protein, and other compounds can contribute to a wide range of hazy appearance within this category. Malt aroma - Flavor: Very low to medium-low malt aroma and flavor may be present, and may exhibit attributes typical of various adjuncts and specialty malts. Hop aroma -  Flavor: Medium to very high hop aroma and flavor are present, with attributes typical of hops from any origin.  Low to very high bitterness. Fermentation - Fruity esters are low to high and may contribute to an overall highly fruity impression regardless of the presence or absence of fruit(s) used and can contribute to the perception of sweetness and be complementary to the hop profile. Yeast choices can vary widely as can sensory outcomes; very low to low phenolic or other attributes typical of wine, champagne or <i>Brettanomyces</i> yeast strains may be present but are not required. Carbonation can range from average to high, with higher levels often associated with a crisp mouthfeel. Diacetyl and DMS should not be present.. Very low to medium, depending on grist and yeast choice, enzymatic treatment, finishing adjunct(s) and other fermentation parameters. Mouthfeel can vary widely from light to full and from dry to silky. body.",
        "moderated": true,
        "minIBU": 30,
        "maxIBU": 100,
        "minABV": 6.3,
        "maxABV": 10.6
    },
    {
        "name": "Historical Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Varies with underlying style. Varies with underlying style. Malt aroma - Flavor: Varies with underlying style. Hop aroma -  Flavor: Varies with underlying style.  Varies with underlying style bitterness. Fermentation - Varies with underlying style. Varies with underlying style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Wild Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Any color is acceptable. Versions made with fruits or other flavorings may take on corresponding hues.. Clear or hazy due to yeast, chill haze or hop haze.. Malt aroma - Flavor: Generally, these beers are highly attenuated resulting in very low to low malt character. Maltier versions should display good overall balance with other flavor components.. Hop aroma -  Flavor: Very low to high.  Very low to low bitterness. Fermentation - Aromas may vary significantly due to fermentation attributes contributed by various known and unknown microorganisms. The overall balance should be complex and balanced. Wild beers are spontaneously fermented with microorganisms that the brewer has introduced from the ambient air/environment near the brewery in which the beer is brewed. Wild Beers may not be fermented with any cultured strains of yeast or bacteria. Wild Beers may or may not be perceived as acidic. They may include a highly-variable spectrum of ﬂavors and aromas derived from the wild microorganisms with which they are fermented. The overall balance of ﬂavors, aromas, appearance and body are important factors in assessing these beers.. Very low to medium body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Smoke Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Any beer of any style incorporating smoke, and therefore may range from very light to black. Varies with underlying beer style. Malt aroma - Flavor: Varies with underlying beer style. Hop aroma -  Flavor: Varies with underlying beer style.  Varies with underlying beer style bitterness. Fermentation - For Smoke Beers based on lager styles, any phenolic notes (if present) should be derived from smoke; in such lagers yeast-derived phenolics should not be present. . Varies with underlying beer style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Other Strong Ale or Lager",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Varies with underlying style. Varies with underlying style. Malt aroma - Flavor: Varies with underlying style. Hop aroma -  Flavor: Varies with underlying style.  Varies with underlying style bitterness. Fermentation - Within the framework of these guidelines, beers of any style intentionally brewed to a higher alcohol content than defined within that style’s guidelines are categorized as Other Strong Beer. These beers should achieve a balance between the style’s characteristics and the additional alcohol, and are not wood- or barrel-aged. All Wood- and Barrel-Aged Beers that meet the criteria for elevated alcohol content shown below are categorized as any of several Wood- and Barrel-Aged Beers.. Varies with underlying style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 8,
        "maxABV": 8
    },
    {
        "name": "Gluten-Free Beer",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Varies with underlying style. Varies with underlying style. Malt aroma - Flavor: Varies with underlying style. Grains and fermentables which differ from those typically used to produce a given beer style can and will produce flavor and aroma outcomes that differ from traditional versions. Such differences are to be expected and are acceptable.. Hop aroma -  Flavor: Varies with underlying style.  Varies with underlying style bitterness. Fermentation - Although brewers may design and identify these beers according to defined style guidelines, these beers should be evaluated on their own merits without strict adherence to defined style parameters.. Varies with underlying style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": 0,
        "maxABV": 0
    },
    {
        "name": "Non-Alcohol Malt Beverage",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Varies with underlying style. Varies with underlying style. Malt aroma - Flavor: Varies with underlying style. Hop aroma -  Flavor: Varies with underlying style.  Varies with underlying style bitterness. Fermentation - Non-alcohol (N/A) malt beverages can emulate the character of any beer style defined within these guidelines but with no or nearly no alcohol <b>(less than 0.5 percent abv)</b>. Ethyl acetate should not be present. Due to their nature, non-alcohol malt beverages will have a proﬁle lacking the complexity and balance of ﬂavors that beers containing alcohol will display. N/A beers should be assessed with this is mind, and should not be given negative evaluations for reasons related to the absence of alcohol.. Varies with underlying style body.",
        "moderated": true,
        "minIBU": null,
        "maxIBU": null,
        "minABV": null,
        "maxABV": null
    },
    {
        "name": "Dessert Stout or Pastry Stout",
        "typing": [
            "Hybrid/Mixed Lagers or Ale",
            "All Origin Hybrid/Mixed Lagers or Ale"
        ],
        "description": "Color - Very dark to black. Opaque. Malt aroma - Flavor: Extremely rich malty aroma and ﬂavor is typical. Coffee, caramel, roasted malt, or chocolate aromas and flavors may be evident.. Hop aroma -  Flavor: If present, very low.  Not present to low bitterness. Fermentation - High alcohol content is evident. Fruity esters may be present at low levels. Diacetyl, if present, should be at low levels.. Full body.",
        "moderated": true,
        "minIBU": 20,
        "maxIBU": 65,
        "minABV": 7,
        "maxABV": 13
    }
]