# BeerScience backend
A simple backend application to experiment with NodeJS deploy

### Local environment
- (Optional) git clone frontend repository
- Copy `.env.example` to `.env` and change values if needed
- Run `docker-compose up` in root directory of backend
- Run migrations, run seeders
- Start to change code: this will auto compile backend and frontend
- Go to [http://localhost:20000](http://localhost:20000) to query backend

### Run migrations
- Run `npx sequelize db:migrate` from backend root directory

### Run seeders
- Run `npx sequelize db:seed:all` from backend root directory

### Healthcheck
Healthcheck is available on HTTP GET `/status` endpoint

## How to deploy
GitlabCI will run pipeline for you:
- On `git push` to `staging` branch with environment variables from `ENV_STG` values that you can change in Gitlab **Settings** => **CI/CD** => **Variables**
